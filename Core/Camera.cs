﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Core
{
    /// <summary>
    /// Provides basic camera, viewport, sizes and matrixes.
    /// </summary>
    public sealed class Camera
    {
        /// <summary>
        /// Width of the game window.
        /// </summary>
        public const int Width = 800;
        /// <summary>
        /// height of the game window.
        /// </summary>
        public const int Height = 496;
        /// <summary>
        /// Size of the tiles in the map.
        /// </summary>
        public const int TileSize = 24;
        /// <summary>
        /// Number of tiles horizontally.
        /// </summary>
        public const int TilesX = 23;
        /// <summary>
        /// Number of tiles vertically.
        /// </summary>
        public const int TilesY = 20;

        /// <summary>
        /// Gets the Viewport Matrix.
        /// </summary>
        public Matrix MapMatrix { get; private set; }
        
        /// <summary>
        /// Refreshed the Matrix of the Viewport.
        /// </summary>
        /// <param name="view">The view to fit the matrix to.</param>
        public void Update(Viewport view)
        {
            int windowX = view.Width,
                windowY = view.Height;
            float fitX = (float) windowX/Width,
                  fitY = (float) windowY/Height,
                  space;

            if (fitX < fitY)
            {
                space = windowY - (Height*fitX);
                MapMatrix = Matrix.CreateScale(fitX)*Matrix.CreateTranslation(new Vector3(0, space/2, 0));
            }
            else
            {
                space = windowX - (Width*fitY);
                MapMatrix = Matrix.CreateScale(fitY)*Matrix.CreateTranslation(new Vector3(space/2, 0, 0));
            }

        }

        /// <summary>
        /// Gets a MouseState with the X and Y values adjusted to the matrix of the camera.
        /// </summary>
        /// <returns>Returns an adjusted instance of the MouseState struct.</returns>
        public MouseState GetMouseState()
        {
            // extract scale and translation.
            Vector3 scale, translation;
            Quaternion rotation;
            MapMatrix.Decompose(out scale, out rotation, out translation);

            MouseState dms = Mouse.GetState();
            return new MouseState(
                (int) ((dms.X - translation.X)/scale.X),
                (int) ((dms.Y - translation.Y)/scale.Y),
                dms.ScrollWheelValue,
                dms.LeftButton,
                dms.MiddleButton,
                dms.RightButton,
                dms.XButton1,
                dms.XButton2
            );
        }
    }
}