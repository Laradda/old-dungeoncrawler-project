﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Core.UI
{
    public class Button 
    {
        protected MouseState PreviousMouseState;
        protected MouseState CurrentMouseState;
        /// <summary>
        /// Triggered when the Button is clicked.
        /// </summary>
        public event EventHandler Clicked;
        /// <summary>
        /// Triggered when the mouse moves over the Button.
        /// </summary>
        public event EventHandler MouseOver;
        /// <summary>
        /// Width of the Button.
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Height of the button.
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Position of the Button.
        /// </summary>
        public Vector2 Position { get; set; }
        /// <summary>
        /// The text to display on the Button.
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Wether the mouse of hovering over the Button.
        /// </summary>
        public bool IsMouseOver { get; protected set; }

        public Texture2D ButtonTexture { get; set; }
        public Texture2D ButtonHoverTexture { get; set; }
        public SpriteFont Font { get; set; }
        public Rectangle? SpriteSource { get; set; }

        public virtual void Load(ContentManager content)
        {
            Font = content.Load<SpriteFont>("Font8");
        }
        /// <summary>
        /// Updates the button state triggering MouseOver and Clicked.
        /// </summary>
        /// <param name="camera"></param>
        public virtual void Update(Camera camera)
        {
            MouseState ms = camera.GetMouseState();
            PreviousMouseState = CurrentMouseState;
            CurrentMouseState = ms;

            if(ms.X > Position.X && ms.Y > Position.Y && ms.X < Position.X+Width && ms.Y < Position.Y + Height)
            {
                if(!IsMouseOver)
                {
                    IsMouseOver = true;
                    if (MouseOver != null)
                        MouseOver(this, EventArgs.Empty);
                }

                if(ms.LeftButton == ButtonState.Released && PreviousMouseState.LeftButton == ButtonState.Pressed)
                {
                    if (Clicked != null)
                        Clicked(this, EventArgs.Empty);
                }
            }else
            {
                IsMouseOver = false;
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

            if(IsMouseOver && ButtonHoverTexture != null)
                spriteBatch.Draw(ButtonHoverTexture, new Rectangle((int)Position.X, (int)Position.Y, Width, Height), SpriteSource, Color.White);
            else if(ButtonTexture != null)
                spriteBatch.Draw(ButtonTexture, new Rectangle((int)Position.X, (int)Position.Y, Width, Height), SpriteSource, Color.White);

            if (Text != null)
            {
                Vector2 textPosition = new Vector2((float) Width/2 - Font.MeasureString(Text).X/2,
                                                   (float) Height/2 - Font.MeasureString(Text).Y/2);
                spriteBatch.DrawString(Font, Text, textPosition + Position, Color.White);
            }
        }
    }
}
