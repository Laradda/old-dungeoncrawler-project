﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Core.UI
{
    public class StatusBar
    {

        protected GraphicsDevice Graphics;

        public StatusBar(GraphicsDevice graphics)
        {
            Graphics = graphics;
        }

        public Texture2D CreateStatusBar(Color fill, int width, int height, float fillPercentage)
        {
            Texture2D texture2D = new Texture2D(Graphics, width, height);

            Color[] pixels = new Color[width * height];
            for (int i = 0, total = width * height; i < total; i++)
            {
                int y = i / width,
                    x = i % width;
                float p = (float)x / width;

                 pixels[i] = p > fillPercentage ? Color.Transparent : fill;

            }
            texture2D.SetData(pixels);

            return texture2D;
        }
        public Texture2D CreateStatusBar(Color fill, int width, int height, float fillPercentage,Color border, int borderWidth)
        {
            Texture2D texture2D = new Texture2D(Graphics, width, height);

            Color[] pixels = new Color[width * height];
            for (int i = 0, total = width * height; i < total; i++)
            {
                int y = i / width,
                    x = i % width;
                float p = (float)x / width;

                // is pixel at border
                if (y < borderWidth || x < borderWidth || x >= width - borderWidth || y >= height - borderWidth)
                    pixels[i] = border;
                else
                    pixels[i] = p > fillPercentage ? Color.Transparent : fill;

            }
            texture2D.SetData(pixels);

            return texture2D;
        }




    }
}