﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Core.Actors.Pawns;
using Core.Actors.Pawns.Creeps;
using Core.Effects;
using Core.Spells;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Effect = Core.Effects.Effect;

namespace Core.UI
{
    public class Interface
    {
        protected SpriteFont Font8;
        protected SpriteFont Font10;
        protected SpriteFont Font14;

        protected GameState GameState;

        protected Texture2D Portraits;
        protected Texture2D Background;
        protected Texture2D Items;

        protected Vector2 UiPosition;

        protected StatusBar StatusBarGenerator;
        protected GraphicsDevice Graphics;

        public List<Button> Buttons { get; private set; }

        // UI buttons
        public Button BtnHealthBar { get; private set; }
        public Button BtnManaBar { get; private set; }
        public Button BtnXpBar { get; private set; }
        public InvisibleButton BtnDamage { get; private set; }
        public InvisibleButton BtnHealthPotion { get; private set; }
        public InvisibleButton BtnManaPotion { get; private set; }

        public Button[] SpellButtons { get; private set; } 

        public Interface(GameState gameState, GraphicsDevice graphics)
        {
            Buttons = new List<Button>();
            SpellButtons = new Button[4];
            GameState = gameState;
            Graphics = graphics;
            UiPosition = new Vector2(Camera.TilesX * Camera.TileSize + 16 + 8, 8);

            BtnHealthBar = new Button { Height = 20, Width = 180, Position = UiPosition + new Vector2(24, 82) };
            BtnManaBar = new Button { Height = 20, Width = 180, Position = UiPosition + new Vector2(24, 101) };
            BtnXpBar = new Button { Height = 10, Width = 180, Position = UiPosition + new Vector2(24, 120) };
            BtnDamage = new InvisibleButton { Height = 24, Width = 24, Position = UiPosition + new Vector2(100, 50) };

            BtnHealthPotion = new InvisibleButton { Width = 24, Height = 24, Position = UiPosition + new Vector2(128, 24) };
            BtnHealthPotion.Clicked += delegate{ GameState.Player.ConsumeHealthPotion(); };
            BtnManaPotion = new InvisibleButton { Width = 24, Height = 24, Position = UiPosition + new Vector2(128, 48) };
            BtnManaPotion.Clicked += delegate { GameState.Player.ConsumeManaPotion(); };

            Buttons.Add(BtnHealthBar);
            Buttons.Add(BtnManaBar);
            Buttons.Add(BtnXpBar);
            Buttons.Add(BtnDamage);
            Buttons.Add(BtnHealthPotion);
            Buttons.Add(BtnManaPotion);
        }

        public virtual void Update(Camera camera)
        {
            foreach (Button b in Buttons)
                b.Update(camera);
            foreach(Button b in SpellButtons)
                b.Update(camera);
        }

        public virtual void Load(ContentManager content)
        {
            Font8 = content.Load<SpriteFont>("Font8");
            Font10 = content.Load<SpriteFont>("Font10");
            Font14 = content.Load<SpriteFont>("Font14");

            Portraits = content.Load<Texture2D>("portraits");
            Background = content.Load<Texture2D>("bg");
            Items = content.Load<Texture2D>("items");

            StatusBarGenerator = new StatusBar(Graphics);


            RedrawStatusBars(GameState.Player);
            foreach (Creep creep in GameState.Objects.OfType<Creep>())
                RedrawStatusBars(creep);

            // load buttons
            foreach (Button b in Buttons)
                b.Load(content);

            for(int i= 0;i<4;i++)
            {
                SpellButtons[i] = new Button{
                    Width = 48,
                    Height = 48,
                    Position = UiPosition+new Vector2(56*i,138),
                    ButtonTexture = content.Load<Texture2D>("spellButton")
                };

                int i1 = i;
                SpellButtons[i].Clicked += delegate
                {
                    if(GameState.Player.Spells[i1] != null)
                    {
                        Spell spell = GameState.Player.Spells[i1];
                        if(spell.SelfCast)
                        {
                            GameState.Player.Cast(spell,GameState.Player);
                        }
                        else
                        {
                            GameState.Player.ActiveSpell = GameState.Player.ActiveSpell == spell ? null : spell;
                        }
                    }

                };
            }

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            RedrawStatusBars(GameState.Player);



            // draw portrait
            spriteBatch.Draw(Portraits, UiPosition, GameState.Player.PortraitSource, Color.White);
            // draw level + title + name
            spriteBatch.DrawString(Font14, GameState.Player.FullName,
                UiPosition + new Vector2(80, 0), Color.White);

            // gold icon + stat
            spriteBatch.Draw(Items, UiPosition + new Vector2(80, 24), new Rectangle(24, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, GameState.Player.Gold.ToString(), UiPosition + new Vector2(100, 28), Color.White);

            // attack icon + stat
            spriteBatch.Draw(Items, UiPosition + new Vector2(80, 48), new Rectangle(48, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, GameState.Player.GetAttackDamage().ToString(), UiPosition + new Vector2(100, 50), Color.White);

            // potions
            spriteBatch.Draw(Items, UiPosition + new Vector2(128, 24), new Rectangle(168, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, GameState.Player.HealthPotions.ToString(), UiPosition + new Vector2(150, 28), Color.White);
            
            spriteBatch.Draw(Items, UiPosition + new Vector2(128, 48), new Rectangle(192, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, GameState.Player.ManaPotions.ToString(), UiPosition + new Vector2(150, 50), Color.White);


            // draw targeted creep
            if (GameState.MouseTarget != null && GameState.MouseTarget is Creep)
                DrawTarget(spriteBatch, (Creep)GameState.MouseTarget);
            else
                DrawStats(spriteBatch, GameState.Player);

            DrawSpells(spriteBatch,GameState.Player);

            foreach(Button b in SpellButtons)
                b.Draw(spriteBatch);

            // draw buttons
            foreach (Button b in Buttons)
                b.Draw(spriteBatch);
        }

        private void DrawStats(SpriteBatch spriteBatch, Player player)
        {
            Vector2 pos = new Vector2(0, 256) + UiPosition;
            DrawEffects(spriteBatch, player.EffectManager, new Vector2(0, 362));
        }

        public virtual void DrawBackGround(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Background, Vector2.Zero, Color.White);
        }

        public virtual void DrawTarget(SpriteBatch spriteBatch, Creep creep)
        {

            RedrawStatusBars(creep);

            spriteBatch.Draw(Portraits, new Vector2(0, 256) + UiPosition, creep.PortraitSource, Color.White);
            spriteBatch.DrawString(Font10, String.Format("Level {0} {1}", creep.Level, creep.Name), new Vector2(80, 256) + UiPosition, Color.White);

            string damage = String.Format("{0}",
                    creep.GetAttackDamage() 
            );
            string health = String.Format("{0} / {1}",
                creep.CurrentHealth,
                creep.BaseHealth
            );

            // attack icon + stat
            spriteBatch.Draw(Items, UiPosition + new Vector2(80, 304), new Rectangle(48, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, damage, UiPosition + new Vector2(100, 306), Color.White);

            // health bar + health text
            spriteBatch.Draw(creep.HealthBar, UiPosition + new Vector2(24, 336), Color.White);
            spriteBatch.DrawString(Font10, health, UiPosition + new Vector2(24 + (192 / 2 - Font10.MeasureString(health).X / 2), 338), Color.White);

            if(creep.MagicAttack)
                spriteBatch.DrawString(Font10, "Magic attack", UiPosition + new Vector2(80, 272), new Color(96,96,255,255));

            DrawEffects(spriteBatch, creep.EffectManager, new Vector2(0, 362));

        }

        public virtual void DrawEffects(SpriteBatch spriteBatch, EffectManager effects, Vector2 position)
        {
            int offset = 0;
            foreach (Effect effect in effects.Effects)
            {
                string effectText = String.Format("{0}: {1}",
                    effect.Name,
                    effect.Description
                );

                spriteBatch.DrawString(Font8, effectText, UiPosition + position + new Vector2(0, offset), Color.White);
                offset += 12;
            }

        }

        protected virtual void DrawSpells(SpriteBatch spriteBatch, Player player)
        {
            int i = 0;
            foreach (Spell spell in GameState.Player.Spells)
            {
                Color color = player.CanCast(spell) ? Color.White : new Color(255,128,128,255);
                if (GameState.Player.ActiveSpell == spell)
                    color = new Color(128, 128, 255, 255);
                string hotKey = (1+i).ToString(CultureInfo.InvariantCulture);

                spriteBatch.Draw(spell.SpriteSheet, new Rectangle((int)SpellButtons[i].Position.X + 5, (int)SpellButtons[i].Position.Y + 5, 38, 38),spell.SpriteSource, color);
                spriteBatch.DrawString(Font14, hotKey, SpellButtons[i].Position + new Vector2(8, 20), Color.Black);

                i++;
            }

        }

        /// <summary>
        /// Redraws status bars of the specified player if needed.
        /// </summary>
        /// <param name="player"></param>
        protected void RedrawStatusBars(Player player)
        {

            Color hpBarColor = player.EffectManager.GetEffect<Poison>() == null ? Color.Red : Color.Purple,
                  mpBarColor = player.EffectManager.GetEffect<ManaBurn>() == null ? Color.Blue : Color.Purple;



            if (player.RedrawHealthBar)
                player.HealthBar = StatusBarGenerator.CreateStatusBar(hpBarColor, 180, 20, (float)player.CurrentHealth / player.BaseHealth, Color.White, 1);
            if (player.RedrawManabar)
                player.ManaBar = StatusBarGenerator.CreateStatusBar(mpBarColor, 180, 20, (float)player.CurrentMana / player.BaseMana, Color.White, 1);
            if (player.RedrawXpBar)
                player.XpBar = StatusBarGenerator.CreateStatusBar(Color.Green, 180, 10, (float)player.Xp / player.XpNeeded, Color.White, 1);

            player.RedrawHealthBar = false;
            player.RedrawManabar = false;
            player.RedrawXpBar = false;

            BtnHealthBar.ButtonTexture = player.HealthBar;
            BtnHealthBar.Text = String.Format("{0} / {1}", player.CurrentHealth, player.BaseHealth);
            BtnManaBar.ButtonTexture = player.ManaBar;
            BtnManaBar.Text = String.Format("{0} / {1}", player.CurrentMana, player.BaseMana);
            BtnXpBar.ButtonTexture = player.XpBar;

        }
        /// <summary>
        /// Redraws status bars of the specified crepe if needed.
        /// </summary>
        /// <param name="creep"></param>
        protected void RedrawStatusBars(Creep creep)
        {
            Color hpBarColor = creep.EffectManager.GetEffect<Poison>() == null ? Color.Red : Color.Purple;


            if (creep.RedrawHealthBar)
                creep.HealthBar = StatusBarGenerator.CreateStatusBar(hpBarColor, 180, 20, (float)creep.CurrentHealth / creep.BaseHealth, Color.White, 1);

            creep.RedrawHealthBar = false;
        }


    }
}
