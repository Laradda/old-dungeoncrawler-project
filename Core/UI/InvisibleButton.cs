﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Core.UI
{
    /// <summary>
    /// An invisible button used for just hover and click events.
    /// </summary>
    public class InvisibleButton :Button
    {
        public override void Load(ContentManager content)
        {
            
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            
        }

    }
}
