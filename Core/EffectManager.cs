﻿using System.Collections.Generic;
using System.Linq;
using Core.Actors.Pawns;
using Core.Effects;

namespace Core
{
    /// <summary>
    /// Manages a collection of effects.
    /// </summary>
    public sealed class EffectManager
    {
        /// <summary>
        /// The list of effects in the EffectManager.
        /// </summary>
        public List<Effect> Effects{ get; private set; }

        /// <summary>
        /// Initializes a new instance of the EffectManager class.
        /// </summary>
        public EffectManager()
        {
            Effects = new List<Effect>();
        }

        /// <summary>
        /// Gets the combined effect of a specified effect type.
        /// </summary>
        /// <typeparam name="T">The type of effect the get.</typeparam>
        /// <returns>A combined effect object; or null if no effects of that type are present in the EffectManager.</returns>
        public T GetEffect<T>() where T : Effect<T>, new ()
        {
            T total = null;
            foreach(T effect in Effects.OfType<T>())
            {
                if(total == null)
                    total = effect;
                else
                    total = effect.Combine(total, effect);
            }
            return total;
        }

        /// <summary>
        /// Removes all the effects of a specific type from the EffectManager.
        /// </summary>
        /// <typeparam name="T">The type of effect the remove.</typeparam>
        public void RemoveEffect<T>() where T : Effect, new()
        {
            List<T> removing = Effects.OfType<T>().ToList();
            foreach (T t in removing)
                Effects.Remove(t);
        }

        public void Tick(Player sender ,TickType type)
        {
            foreach(Effect effect in Effects)
            {
                effect.OnTick(sender,new TickEventArgs(type,effect));
            }
        }
    }
}