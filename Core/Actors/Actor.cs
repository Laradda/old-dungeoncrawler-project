﻿using System;
using Core.Actors.Pawns;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Core.Actors
{
    /// <summary>
    /// The base class of every visible object in the game.
    /// </summary>
    public abstract class Actor : IDisposable
    {
        /// <summary>
        /// Triggered when this actor is found by a player.
        /// </summary>
        public event PlayerEventHandler Found;

        /// <summary>
        /// Triggered when this actor collides with a player.
        /// </summary>
        public event PlayerEventHandler Collide;

        /// <summary>
        /// Zero-based position of the actor.
        /// </summary>
        public Point Position { get; set; }
        /// <summary>
        /// The spritesheet used for drawing this actor.
        /// </summary>
        public Texture2D SpriteSheet { get;protected set; }
        /// <summary>
        /// The source in the spritesheet which represends this actor.
        /// </summary>
        public Rectangle SpriteSource { get; protected set; }

        public Color Color { get; set; }
        /// <summary>
        /// The actor is visible and should be drawn.
        /// </summary>
        public bool Visible { get; protected set; }
        /// <summary>
        /// this actor is about to be deleted.
        /// </summary>
        public bool DeleteMe { get; protected set; }
        /// <summary>
        /// The actor blocks other actors.
        /// </summary>
        public bool Obstacle { get; protected set; }

        public bool Hover { get; set; }

        /// <summary>
        /// Initializes a new instance of the Actor class.
        /// </summary>
        protected Actor()
        {
            Color = Color.White;
        }

        /// <summary>
        /// Called when this actor collides with a player.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="e"></param>
        internal void OnCollide(Player player, EventArgs e)
        {
            if (Collide != null) Collide(player, e);
        }

        /// <summary>
        /// Called when this actor is found by a player.
        /// </summary>
        /// <param name="player">The player this actor collides with.</param>
        internal void OnFound(Player player, EventArgs e)
        {
            if (Visible)
                return;
            Visible = true;
            if (Found != null) Found(player, e);
        }
        /// <summary>
        /// Initializes the actor, called when the Actor is constructed and contained in the GameState object.
        /// </summary>
        public virtual void Init()
        {
        }
        /// <summary>
        /// Loads content into the actor.
        /// </summary>
        /// <param name="content">ContentManager provided by the game.</param>
        public virtual void Load(ContentManager content)
        {
        }
        /// <summary>
        /// Updates the actor.
        /// </summary>
        public virtual void Update()
        {
        }
        /// <summary>
        /// Draws the actor.
        /// </summary>
        /// <param name="spriteBatch">Spritebatch provided by the game.</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(SpriteSheet,
                new Rectangle(Position.X * Camera.TileSize + 8, Position.Y * Camera.TileSize + 8, Camera.TileSize, Camera.TileSize),
                SpriteSource, Color);
        }

        public virtual void Dispose()
        {
            
        }
    }
}
