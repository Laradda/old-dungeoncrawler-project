﻿using System;
using Core.Actors.Pawns;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Core.Actors.Tiles
{
    public abstract class Tile : Actor
    {
        protected Tile()
        {

        }



        public override void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("tiles");
            base.Load(content);
        }

    }
}
