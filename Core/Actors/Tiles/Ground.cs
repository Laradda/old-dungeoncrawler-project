﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Tiles
{
    public class Ground : Tile
    {
        public Ground()
        {
            SpriteSource = new Rectangle(320, 0, 32, 32);
        }
    }
}
