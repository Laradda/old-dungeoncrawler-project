﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Tiles
{
    public class Wall : Tile
    {
        public Wall()
        {
            Obstacle = true;
            //TextureSource = new Rectangle(224, 0, 32, 32);
            SpriteSource = new Rectangle(64,0,32,32);
        }
    }
}
