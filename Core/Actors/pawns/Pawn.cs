﻿using System;
using System.Collections.Generic;
using Core.Effects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Effect = Core.Effects.Effect;
using Core.Actors.Pawns.Creeps;

namespace Core.Actors.Pawns
{
    /// <summary>
    /// The base class of every actor that participates in combat.
    /// </summary>
    public abstract class Pawn : Actor
    {
        private int _currentHealth;
        private int _baseHealth;

        /// <summary>
        /// Wether the health bar needs to be redrawn.
        /// </summary>

        public bool RedrawHealthBar { get; set; }
        /// <summary>
        /// Gets the health bar texture
        /// </summary>
        public Texture2D HealthBar { get; set; }

        /// <summary>
        /// The base damage.
        /// </summary>
        public int BaseDamage { get; set; }
        /// <summary>
        /// The current health of the pawn.
        /// </summary>
        public int CurrentHealth
        {
            get { return _currentHealth; }
            protected set
            {
                _currentHealth = value;
                RedrawHealthBar = true;
                if (_currentHealth <= 0)
                    Dead = true;
            }
        }
        /// <summary>
        /// The maximum health of the pawn.
        /// </summary>
        public int BaseHealth
        {
            get { return _baseHealth; }
            set
            {
                _baseHealth = value;
                RedrawHealthBar = true;
            }
        }

        /// <summary>
        /// The level of the pawn.
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Contains all the effect for the pawn.
        /// </summary>
        public EffectManager EffectManager { get; protected set; }
        /// <summary>
        /// Spritesheet used for drawing a portait for the pawn.
        /// </summary>
        public Texture2D PortraitSprite { get; protected set; }
        /// <summary>
        /// The source in the portait spritesheet which represends this actor.
        /// </summary>
        public Rectangle PortraitSource { get; protected set; }
        /// <summary>
        /// The name of the pawn
        /// </summary>
        public string Name { get;protected set; }
        /// <summary>
        /// Indicates wether the pawn is dead or not.
        /// </summary>
        public bool Dead { get;protected set; }

        /// <summary>
        /// Gets a full name of the pawn including the level.
        /// </summary>
        public virtual string FullName
        {
            get { return string.Format("Level {0} {1}", Level, Name); }
        }

        /// <summary>
        /// Initializes a new instance of the Pawn class.
        /// </summary>
        protected Pawn()
        {
            EffectManager = new EffectManager();
            RedrawHealthBar = true;
        }
        public override void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("creatures");
            PortraitSprite = content.Load<Texture2D>("portraits");
            base.Load(content);
        }

        /// <summary>
        /// Regenerates from discovered tiles.
        /// </summary>
        public virtual void Regenerate()
        {
            if (CurrentHealth >= BaseHealth) return;

            if(EffectManager.GetEffect<Poison>() == null)
                RegenerateHealth();
        }

        protected virtual void RegenerateHealth()
        {
            // regenerate at least 1 hp.
            CurrentHealth += GetHealtRegeneration();

            // dont exceed max
            CurrentHealth = Math.Min(CurrentHealth, BaseHealth);
        }
        protected virtual int GetHealtRegeneration()
        {
            int amount = Math.Max(1, BaseHealth/10);

            HealthRecoverBonus bonus = EffectManager.GetEffect<HealthRecoverBonus>();
            if(bonus != null)
            {
                amount += amount * (bonus.Amount / 100);
            }
            return amount;
        }

        /// <summary>
        /// Calculates the attack damage for this current pawn against a specified target pawn.
        /// </summary>
        /// <param name="target">The target to calculate against.</param>
        /// <returns></returns>
        public virtual int GetAttackDamageTarget(Pawn target)
        {
            int damage = GetAttackDamage();

            // first strike bonus
            if (this is Player && target is Creep)
            {

                Creep creep = (Creep) target;
                Player player = (Player) this;

                if (player.GetFirstStrike(creep))
                {
                    DamageBonusFirstStrike damageBonusFirstStrike = EffectManager.GetEffect<DamageBonusFirstStrike>();
                    if(damageBonusFirstStrike != null)
                    {
                        int bonus = damage*(damageBonusFirstStrike.Amount/100);
                        damage += bonus;
                    }
                }
            }





            PhysicalResistance resistance = target.EffectManager.GetEffect<PhysicalResistance>();
            if (resistance != null)
            {
                float percent = (float)resistance.Amount/100;
                float sub = damage*percent;
                damage -= (int)sub;
            }
            return damage;
        }


        /// <summary>
        /// Called when the this pawn just dealt damage to an other pawn.
        /// </summary>
        /// <param name="pawn"></param>
        internal virtual void HitOther(Pawn pawn)
        {

            // apply poison
            PoisonAttack pa = EffectManager.GetEffect<PoisonAttack>();
            if (pa != null)
            {
                pawn.EffectManager.Effects.Add(new Poison{ Name = "Poison" });
            }

            // apply mana burn
            if (pawn is Player) // can only mana burn players.
            {
                if (EffectManager.GetEffect<ManaBurnAttack>() != null)
                {
                    pawn.EffectManager.Effects.Add(new ManaBurn{Name = "Mana burn" });
                    ((Player) pawn).CurrentMana = 0;
                }
            }
        }

        /// <summary>
        /// Calculates the attack damage for this current pawn.
        /// </summary>
        /// <returns></returns>
        public virtual int GetAttackDamage()
        {
           
            int damage = BaseDamage;

            // damage bonus
            DamageBonus bonus = EffectManager.GetEffect<DamageBonus>();
            if (bonus != null)
            {
                float percent = (float)bonus.Amount / 100;
                float sub = damage * percent;
                damage += (int)sub;
            }

            return damage;
        }

        public override void Update()
        {
            List<Effect> deleteMe = new List<Effect>();
            foreach (Effect effect in EffectManager.Effects)
            {
                if(effect.Expires && effect.Duration < 1)
                {
                    deleteMe.Add(effect);
                }
            }
            foreach (Effect effect in deleteMe)
            {
                EffectManager.Effects.Remove(effect);
            }

            base.Update();
        }
        public override void Dispose()
        {
            if (HealthBar != null)
            HealthBar.Dispose();
            base.Dispose();
        }
    }
}
