﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Vampire : Creep
    {
        public Vampire()
        {
            SpriteSource = new Rectangle(64, 32, 32, 32);
            PortraitSource = new Rectangle(72, 288, 72, 72); Name = "Vampire";
            MagicAttack = true;
            EffectManager.Effects.Add(new ManaBurnAttack() { Name = "Mana burn" });

        }




    }
}
