﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Beetle : Creep
    {




        public Beetle()
        {
            SpriteSource = new Rectangle(32, 0, 32, 32);
            PortraitSource = new Rectangle(288, 216, 72, 72); Name = "Beetle";
        }
        public override void Init()
        {
            EffectManager.Effects.Add(new PhysicalResistance
            {
                 Amount = 15,
                 Name = "Hard shell",
                 
            });
            base.Init();
        }
    }
}
