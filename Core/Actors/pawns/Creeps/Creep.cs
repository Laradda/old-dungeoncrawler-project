﻿using System;
using Core.Effects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Core.Actors.Pawns.Creeps
{
    public abstract class Creep : Pawn
    {
        /// <summary>
        /// Triggered after a players hits this target with a physical attack.
        /// This is the last stage of the combat routine of the player.
        /// </summary>
        public event PlayerEventHandler Hit;

        public SpriteFont Font8;
        public SpriteFont Font14;

        public bool MagicAttack { get; set; }

        protected Creep()
        {
            Obstacle = true;
            Collide += CreepCombat;
        }

        private void CreepCombat(Player sender, EventArgs e)
        {
            if(sender.GetFirstStrike(this))
            {
                sender.HitCreep(this);
                if (!Dead)
                    HitPlayer(sender);
            }
            else
            {
                HitPlayer(sender);
                if (!sender.Dead)
                    sender.HitCreep(this);
            }
        }

        public void OnHit(Player player, EventArgs e)
        {
            if (Hit != null) Hit(player, e);
        }

        public override void Init()
        {
            BaseHealth = Level*4;
            CurrentHealth = BaseHealth;
            BaseDamage = Level*3;
            base.Init();
        }

        public override void Load(ContentManager content)
        {
            Font8 = content.Load<SpriteFont>("Font8");
            Font14 = content.Load<SpriteFont>("Font14");
            base.Load(content);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            if(Hover)
            {
                spriteBatch.DrawString(Font14, Level.ToString(), new Vector2(Position.X * Camera.TileSize + 8, Position.Y * Camera.TileSize + 8), Color.White);
            }
            else
            {
                spriteBatch.DrawString(Font8, Level.ToString(), new Vector2(Position.X * Camera.TileSize + 8, Position.Y * Camera.TileSize + 8), Color.White);
            }
        }

        internal virtual void HitPlayer(Player player)
        {
            Avoid avoid = player.EffectManager.GetEffect<Avoid>();
            if (avoid != null && avoid.Roll(avoid.Chance))
            {

                // attack avoided
                return;
            }


            player.DealDamage(GetAttackDamageTarget(player));
            HitOther(player);
            player.EffectManager.Tick(player, TickType.DamageTaken);
        }

        public override int GetAttackDamageTarget(Pawn target)
        {
            if (MagicAttack)
                return GetMagicAttackDamageTarget(target);
            else
                return base.GetAttackDamageTarget(target);
        }

        public virtual int GetMagicAttackDamageTarget(Pawn target)
        {
            int damage = GetAttackDamage();

            MagicResistance resistance = target.EffectManager.GetEffect<MagicResistance>();
            if (resistance != null)
            {
                float percent = (float)resistance.Amount / 100;
                float sub = damage * percent;
                damage -= (int)sub;
            }
            return damage;

        }
        
        public virtual void DealDamage(int damage, Player player)
        {
            CurrentHealth -= damage;
            if(Dead)
                player.KilledCreep(this);
        }
    }
}
