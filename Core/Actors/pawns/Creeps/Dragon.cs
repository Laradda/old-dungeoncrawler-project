﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Dragon : Creep
    {
        public Dragon()
        {
            SpriteSource = new Rectangle(96, 0, 32, 32);
            PortraitSource = new Rectangle(0, 216, 72, 72); 
            Name = "Dragon";
            MagicAttack = true;
        }

        public override void Init()
        {
            EffectManager.Effects.Add(new PhysicalResistance()
            {
                Amount = 20,
                Name = "Dragon skin"
                
            });
            EffectManager.Effects.Add(new MagicResistance()
            {
                Amount = 20,
                Name = "Dragon skin"
            });
            base.Init();
        }



    }
}
