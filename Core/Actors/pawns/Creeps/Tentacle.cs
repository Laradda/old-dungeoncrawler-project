﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Tentacle:Creep
    {
        public Tentacle()
       {
           SpriteSource = new Rectangle(0,64,32,32);
           PortraitSource = new Rectangle(216, 288, 72, 72);
            Name = "Tentacle";
       }
        public override void Init()
        {
            EffectManager.Effects.Add(new PhysicalResistance
            {
                Amount = 20,
                Name = "Thick skin"
                
            });
            base.Init();
        }
    }
}
