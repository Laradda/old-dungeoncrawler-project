﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Orc :Creep
    {
        public Orc()
       {
           SpriteSource = new Rectangle(160,0,32,32);
           PortraitSource = new Rectangle(72, 360, 72, 72);
            Name = "Orc";
       }


    }
}
