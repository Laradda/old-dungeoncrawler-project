﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class BileDemon : Creep
    {
        public BileDemon()
        {
            SpriteSource = new Rectangle(64, 64, 32, 32);
            PortraitSource = new Rectangle(216, 216, 72, 72);
            Name = "Bile Demon";
        }




    }
}
