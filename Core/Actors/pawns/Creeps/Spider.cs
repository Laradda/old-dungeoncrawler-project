﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Spider : Creep
    {
        public Spider()
        {
            SpriteSource = new Rectangle(128, 64, 32, 32);
            PortraitSource = new Rectangle(288, 288, 72, 72); Name = "Spider";
            EffectManager.Effects.Add(new PoisonAttack() { Name = "Poison" });
        }




    }
}
