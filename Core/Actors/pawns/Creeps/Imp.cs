﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Imp:Creep
    {
        public Imp()
       {
           SpriteSource = new Rectangle(0,0,32,32);
           PortraitSource = new Rectangle(144, 360, 72, 72); 
            Name = "Imp";
       }

    }
}
