﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class HornedReaper : Creep
    {
        public HornedReaper()
        {
            SpriteSource = new Rectangle(96, 64, 32, 32);
            PortraitSource = new Rectangle(288, 360, 72, 72); Name = "Horned Reaper";
        }




    }
}
