﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Fly :Creep
    {
        public Fly()
        {
            SpriteSource = new Rectangle(32, 64, 32, 32);
            PortraitSource = new Rectangle(288, 144, 72, 72); Name = "Fly";
        }



    }
}
