﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Wizard : Creep
    {
        public Wizard()
        {
            SpriteSource = new Rectangle(128, 0, 32, 32);
            PortraitSource = new Rectangle(0, 288, 72, 72); Name = "Wizard";
            MagicAttack = true;
        }



    }
}
