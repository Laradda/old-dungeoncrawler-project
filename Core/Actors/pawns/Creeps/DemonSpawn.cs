﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class DemonSpawn : Creep
    {
        public DemonSpawn()
        {
            SpriteSource = new Rectangle(96, 32, 32, 32);
            PortraitSource = new Rectangle(72, 216, 72, 72);
            Name = "Demon Spawn";
        }

        public override void Init()
        {
            EffectManager.Effects.Add(new FirstStrike
            {
                Name = "Quickness"
                
            });
            base.Init();
        }

    }
}
