﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Skeleton : Creep
    {
        public Skeleton()
        {
            SpriteSource = new Rectangle(64, 0, 32, 32);
            PortraitSource = new Rectangle(0, 360, 72, 72); Name = "Skeleton";
        }


        public override void Init()
        {
            EffectManager.Effects.Add(new MagicResistance()
            {
                Amount = 15,
                Name = "Bones",
                
            });
            base.Init();
        }

    }
}
