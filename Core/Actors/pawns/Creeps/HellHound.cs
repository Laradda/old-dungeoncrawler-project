﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class HellHound : Creep
    {
        public HellHound()
        {
            SpriteSource = new Rectangle(32, 32, 32, 32);
            PortraitSource = new Rectangle(216, 360, 72, 72); 
            Name = "Hell Hound";
            MagicAttack = true;
        }




    }
}
