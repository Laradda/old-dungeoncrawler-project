﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Troll:Creep
    {
        public Troll()
       {
           SpriteSource = new Rectangle(0,32,32,32);
           PortraitSource = new Rectangle(144, 288, 72, 72);
            Name = "Troll";
       }

    }
}
