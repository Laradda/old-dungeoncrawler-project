﻿using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class Ghost : Creep
    {
        public Ghost()
        {
            SpriteSource = new Rectangle(160, 32, 32, 32);
            PortraitSource = new Rectangle(216, 144, 72, 72);
            Name = "Ghost";
        }


        public override void Init()
        {
            EffectManager.Effects.Add(new PhysicalResistance
            {
                Amount = 50,
                Name = "Ghost",
                
            });
            base.Init();
        }


    }
}
