﻿using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Creeps
{
    public class DarkMistress:Creep
    {
        public DarkMistress()
        {
            SpriteSource = new Rectangle(128, 32, 32, 32);
            PortraitSource = new Rectangle(144, 216, 72, 72); 
            Name = "Dark Mistress";
            MagicAttack = true;
        }
    }
}
