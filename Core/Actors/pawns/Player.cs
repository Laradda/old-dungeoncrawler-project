﻿using System;
using System.Collections.Generic;
using Core.Actors.Pawns.Creeps;
using Core.Effects;
using Core.Spells;
using DungeonCrawler.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Core.Actors.Pawns
{
    public abstract class Player : Pawn
    {
        private int _currentMana;
        private int _baseMana;
        private int _xp;
        private int _xpNeeded;

        /// <summary>
        /// Wether the mana bar needs to be redrawn.
        /// </summary>
        public bool RedrawManabar { get; set; }

        /// <summary>
        /// Wether the xp bar needs to be redrawn.
        /// </summary>
        public bool RedrawXpBar { get; set; }

        /// <summary>
        /// Gets the mana bar texture
        /// </summary>
        public Texture2D ManaBar { get; set; }

        /// <summary>
        /// Gets the xp bar texture
        /// </summary>
        public Texture2D XpBar { get; set; }

        /// <summary>
        /// Triggered when the player levels up.
        /// </summary>
        public event PlayerEventHandler LevelUp;

        /// <summary>
        /// Occurs when the player stepped in a specific direction.
        /// </summary>
        public event PlayerEventHandler<StepEventArgs> Step;

        /// <summary>
        /// Occurs when the player walks in a specific direction.
        /// </summary>
        public event PlayerEventHandler<StepEventArgs> Walk;

        protected KeyboardState CurrentKeyState;
        protected KeyboardState PreviousKeyState;
        public int HealthPotions { get; set; }
        public int ManaPotions { get; set; }

        /// <summary>
        /// Current mana.
        /// </summary>
        public int CurrentMana
        {
            get { return _currentMana; }
            set
            {
                _currentMana = value;
                RedrawManabar = true;
            }
        }

        /// <summary>
        /// Maximum mana.
        /// </summary>
        public int BaseMana
        {
            get { return _baseMana; }
            set
            {
                _baseMana = value;
                RedrawManabar = true;
            }
        }

        /// <summary>
        /// Current xp.
        /// </summary>
        public int Xp
        {
            get { return _xp; }
            set
            {
                _xp = value;
                RedrawXpBar = true;
                if(_xp >= _xpNeeded)
                {
                    _xp -= _xpNeeded;
                    Level++;
                    OnLevelUp(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Xp needed to level up.
        /// </summary>
        public int XpNeeded
        {
            get { return _xpNeeded; }
            set
            {
                _xpNeeded = value;
                RedrawXpBar = true;
            }
        }

        /// <summary>
        /// Amount of gold.
        /// </summary>
        public int Gold { get; set; }

        /// <summary>
        /// Current available spells.
        /// </summary>
        public List<Spell> Spells { get; protected set; }

        public Spell ActiveSpell { get; set; }
        /// <summary>
        /// Initializes a new instance of the Player class.
        /// </summary>
        protected Player()
        {
            Spells = new List<Spell>(4);
            RedrawManabar = true;
            RedrawXpBar = true;
            CurrentKeyState = Keyboard.GetState();
            Step += delegate(Player sender, StepEventArgs e) { Position = e.Target; };
            LevelUp += PlayerLevelUp;

            Level = 1;

            // default 
            BaseHealth = 10;
            CurrentHealth = 10;
            
            BaseDamage = 5;
            BaseMana = 10;
            CurrentMana = 10;
            XpNeeded = 5;
            HealthPotions = 1;
            ManaPotions = 1;
        }
        protected virtual void PlayerLevelUp(Player sender, EventArgs eventArgs)
        {
            XpNeeded = Level*5;
            BaseHealth += 10;
            CurrentHealth = BaseHealth;
            CurrentMana = BaseMana;
            BaseDamage += 5;

            // cleanse!
            EffectManager.RemoveEffect<Poison>();
            EffectManager.RemoveEffect<ManaBurn>();

        }
        public override void Update()
        {
            PreviousKeyState = CurrentKeyState;
            CurrentKeyState = Keyboard.GetState();

            HandleMovement();

            base.Update();
        }
        protected void HandleMovement()
        {

            if (Walk == null) // Don't bother checking, no event anyway.
                return;

            Point targetStep = Position;

            if (PreviousKeyState.IsKeyUp(Keys.Left) && CurrentKeyState.IsKeyDown(Keys.Left))
                targetStep = new Point(Position.X - 1, Position.Y);
            else if (PreviousKeyState.IsKeyUp(Keys.Right) && CurrentKeyState.IsKeyDown(Keys.Right))
                targetStep = new Point(Position.X + 1, Position.Y);
            else if (PreviousKeyState.IsKeyUp(Keys.Up) && CurrentKeyState.IsKeyDown(Keys.Up))
                targetStep = new Point(Position.X, Position.Y - 1);
            else if (PreviousKeyState.IsKeyUp(Keys.Down) && CurrentKeyState.IsKeyDown(Keys.Down))
                targetStep = new Point(Position.X, Position.Y + 1);

            if (targetStep != Position)
                OnWalk(new StepEventArgs(targetStep));
        }
        internal void OnLevelUp(EventArgs e)
        {
            if (LevelUp != null) LevelUp(this, e);
        }
        internal void OnWalk(StepEventArgs e)
        {
            if (Walk != null) Walk(this, e);
        }
        internal void OnStep(StepEventArgs e)
        {
            if (Step != null) Step(this, e);
        }
        public virtual int GetManaCost(Spell spell)
        {
            return spell.ManaCost;
        }
        public virtual bool CanCast(Spell spell)
        {
            return CurrentMana >= GetManaCost(spell);
        }
        public virtual void SpellCasted(Spell spell, Actor target)
        {
            CurrentMana -= GetManaCost(spell);
            ActiveSpell = null;
        }
        public virtual void Cast(Spell spell, Actor target)
        {
            if(CanCast(spell))
            {
                bool succes = spell.Cast(this, target);
                if (succes)
                {
                    SpellCasted(spell, target);
                }
            }
        }
        public virtual void HitCreep(Creep creep)
        {

            creep.DealDamage(GetAttackDamageTarget(creep), this);
            
            // critical strike effect
            CriticalStrike cs = EffectManager.GetEffect<CriticalStrike>();
            if(cs != null && cs.Roll(cs.Chance))
                creep.DealDamage(GetAttackDamageTarget(creep), this);

            HitOther(creep);

            EffectManager.Tick(this, TickType.DamageDone);

            creep.OnHit(this,EventArgs.Empty);
        }
        public virtual bool GetFirstStrike(Creep creep)
        {
            // creep has first strike => he goes first.
            if (creep.EffectManager.GetEffect<FirstStrike>() != null)
                return false;

            // you have first strike => you go first.
            if (EffectManager.GetEffect<FirstStrike>() != null)
                return true;

            // you must be a higher level to go first.
            return Level > creep.Level;

        }
        public virtual void KilledCreep(Creep creep)
        {
            Xp += GetXpFromCreep(creep);
        }
        public virtual int GetXpFromCreep(Creep creep)
        {
            return creep.Level;
        }
        public override void Regenerate()
        {
            base.Regenerate();

            if (CurrentMana >= BaseMana) return;

            if(EffectManager.GetEffect<ManaBurn>() == null)
                RegenerateMana();
        }
        protected virtual void RegenerateMana()
        {
            // regenerate at least 1 mp.
            CurrentMana += GetManaRegeneration();

            CurrentMana = Math.Min(CurrentMana, BaseMana);

        }
        protected virtual int GetManaRegeneration()
        {
            int amount = 1;

            ManaRecoverBonus bonus = EffectManager.GetEffect<ManaRecoverBonus>();
            if (bonus != null)
            {
                amount += amount * (bonus.Amount / 100);
            }
            return amount;
        }
        internal override void HitOther(Pawn pawn)
        {
            pawn.EffectManager.RemoveEffect<Poison>();

            base.HitOther(pawn);
        }
        protected internal virtual void ConsumeManaPotion()
        {
            if(ManaPotions > 0)
            {
                CurrentMana += GetManaPotionRestoreAmount();
                CurrentMana = Math.Min(CurrentMana, BaseMana);
                ManaPotions--;
                EffectManager.RemoveEffect<ManaBurn>();
            }
        }
        protected virtual int GetManaPotionRestoreAmount()
        {
            return (int)(BaseMana/2.5);
        }
        protected virtual int GetHealthPotionsResoreAmount()
        {
            return BaseHealth / 2;
        }
        protected internal virtual void ConsumeHealthPotion()
        {
            if (HealthPotions > 0)
            {
                CurrentHealth += GetManaPotionRestoreAmount();
                CurrentHealth = Math.Min(CurrentHealth, BaseHealth);
                HealthPotions--;
                EffectManager.RemoveEffect<Poison>();
            }

        }
        public override void Load(ContentManager content)
        {
            foreach(Spell spell in Spells)
            {
                spell.Load(content);
            }
            base.Load(content);
        }
        public override void Dispose()
        {
            if (ManaBar != null)
                ManaBar.Dispose();
            if (ManaBar != null)
                XpBar.Dispose();
            base.Dispose();
        }
        public virtual void DealDamage(int damage)
        {
            CurrentHealth -= damage;
            CurrentHealth = (int)MathHelper.Clamp(CurrentHealth, 0, BaseHealth);
        }

    }
}