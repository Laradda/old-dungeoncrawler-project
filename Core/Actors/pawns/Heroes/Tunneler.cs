﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Tunneler : Player
    {
        public Tunneler()
        {
            SpriteSource = new Rectangle(96, 128, 32, 32);
            PortraitSource = new Rectangle(216, 72, 72, 72);
            Name = "Tunneler";
            EffectManager.Effects.Add(new Avoid
            {
                Name = "Small",
                Chance = 20

                
            });
            
        }
    }
}
