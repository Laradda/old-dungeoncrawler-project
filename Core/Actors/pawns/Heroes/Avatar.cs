﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Avatar : Player
    {
        public Avatar()
        {
            SpriteSource = new Rectangle(0, 128, 32, 32);
            PortraitSource = new Rectangle(0, 72, 72, 72);
            Name = "Avatar";
            Spells.Add(new Block());
        }
    }
}
