﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Priestess : Player
    {
        public Priestess()
        {
            SpriteSource = new Rectangle(160, 64, 32, 32);
            PortraitSource = new Rectangle(72, 144, 72, 72);
            Name = "Priestess";
            Spells.Add(new Drain());
        }
    }
}
