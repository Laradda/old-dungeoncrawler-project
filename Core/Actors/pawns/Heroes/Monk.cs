﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Effects;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Monk : Player
    {
        public Monk()
        {
            SpriteSource = new Rectangle(32, 96, 32, 32);
            PortraitSource = new Rectangle(0, 0, 72, 72);
            Name = "Monk";
            EffectManager.Effects.Add(new DamageBonus
            {
                Name = "Unarmed",
                Amount = -50
            });
            EffectManager.Effects.Add(new PhysicalResistance
            {
                Name = "Resillient",
                Amount = 50
            });
            EffectManager.Effects.Add(new MagicResistance
            {
                Name = "Resillient",
                Amount = 50
            });
            Spells.Add(new Heal());
        }
    }
}
