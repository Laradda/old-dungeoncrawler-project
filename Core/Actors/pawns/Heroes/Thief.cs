﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Thief : Player
    {
        public Thief()
        {
            SpriteSource = new Rectangle(128, 128, 32, 32);
            PortraitSource = new Rectangle(288, 72, 72, 72);
            Name = "Thief";
            Spells.Add(new HeroicStrike());
        }
    }
}
