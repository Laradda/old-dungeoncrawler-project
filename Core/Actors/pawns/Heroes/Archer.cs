﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Effects;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Archer : Player
    {
        public Archer()
        {
            SpriteSource = new Rectangle(32, 128, 32, 32);
            PortraitSource = new Rectangle(72, 72, 72, 72);
            Name = "Archer";
            Spells.Add( new Shoot());
        }
    }
}
