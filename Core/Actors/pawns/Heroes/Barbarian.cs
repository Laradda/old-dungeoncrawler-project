﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Barbarian : Player
    {
        public Barbarian()
        {
            SpriteSource = new Rectangle(160, 96, 32, 32);
            PortraitSource = new Rectangle(288, 0, 72, 72);
            Name = "Barbarian";
            Spells.Add(new Block());
        }
    }
}
