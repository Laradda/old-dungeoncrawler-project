﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Fairy : Player
    {
        public Fairy()
        {
            SpriteSource = new Rectangle(128, 96, 32, 32);
            PortraitSource = new Rectangle(216, 0, 72, 72);
            Name = "Fairy";
            Spells.Add(new Lightning());
        }
    }
}
