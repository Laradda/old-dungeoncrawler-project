﻿
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Giant : Player
    {
        public Giant()
        {
            SpriteSource = new Rectangle(96, 96, 32, 32);
            PortraitSource = new Rectangle(144, 0, 72, 72);
            Name = "Giant";
            Spells.Add(new HeroicStrike());
        }
    }
}
