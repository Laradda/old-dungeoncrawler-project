﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Effects;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Knight : Player
    {
        public Knight()
        {
            SpriteSource = new Rectangle(64, 96, 32, 32);
            PortraitSource = new Rectangle(72, 0, 72, 72);
            Name = "Knight";

            EffectManager.Effects.Add(new PhysicalResistance{
                Name = "Heavy armor",
                Amount = 25
            });
            Spells.Add(new HeroicStrike());
        }


    }
}
