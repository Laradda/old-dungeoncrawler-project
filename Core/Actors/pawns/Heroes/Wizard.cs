﻿using Core.Effects;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Wizard : Player
    {
        public Wizard()
        {
            SpriteSource = new Rectangle(64, 128, 32, 32);
            PortraitSource = new Rectangle(144, 72, 72, 72);
            Name = "Wizard";
            EffectManager.Effects.Add(new DamageBonus
            {
                Name = "Caster",
                Amount = -50
            });
            EffectManager.Effects.Add(new ManaRecoverBonus(){Amount = 100,Name="Fast recover"});
            Spells.Add(new FireBall(){Name = "Fireball"});
            BaseMana += 5;
        }

    }
}
