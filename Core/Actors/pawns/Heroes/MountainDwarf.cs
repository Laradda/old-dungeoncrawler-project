﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class MountainDwarf : Player
    {
        public MountainDwarf()
        {
            SpriteSource = new Rectangle(0, 96, 32, 32);
            PortraitSource = new Rectangle(144, 144, 72, 72);
            Name = "Mountain Dwarf";
        }
    }
}
