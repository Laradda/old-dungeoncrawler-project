﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Effects;
using Core.Spells;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pawns.Heroes
{
    public class Samurai : Player
    {
        public Samurai()
        {
            SpriteSource = new Rectangle(160, 128, 32, 32);
            PortraitSource = new Rectangle(0, 144, 72, 72);
            Name = "Samurai";
            Spells.Add(new ApplyPoison());
        }
    }
}
