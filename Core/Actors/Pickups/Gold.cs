﻿using Core.Actors.Pawns;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pickups
{
    public class Gold :Pickup
    {
        /// <summary>
        /// The amount of gold contained in this gold pickup.
        /// </summary>
        public int Value { get; set; }
        /// <summary>
        /// Initializes a new instance of the Gold class.
        /// </summary>
        public Gold()
        {
            SpriteSource = new Rectangle(0, 0, 24, 24);
        }

        public override void HandlePickupQuery(Player player)
        {
            player.Gold = player.Gold + Value;

            base.HandlePickupQuery(player);
        }

    }
}
