﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pickups
{
    public class ManaPotion : Pickup
    {

        public ManaPotion()
        {
            SpriteSource = new Rectangle(192, 0, 24, 24);
        }

        public override void HandlePickupQuery(Pawns.Player player)
        {
            player.ManaPotions++;
            base.HandlePickupQuery(player);
        }
    }
}
