﻿using Core.Actors.Pawns;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pickups
{
    public class HealthBoost : Pickup
    {
        public HealthBoost()
        {
            SpriteSource = new Rectangle(96, 0, 24, 24);

        }

        public override void HandlePickupQuery(Player player)
        {
            player.BaseHealth += player.Level;
            base.HandlePickupQuery(player);
        }
    }
}
