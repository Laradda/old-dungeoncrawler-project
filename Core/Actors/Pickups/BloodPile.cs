﻿using Core.Actors.Pawns;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pickups
{
    public class BloodPile : Pickup
    {

        public BloodPile()
        {
            SpriteSource = new Rectangle(72, 0, 24, 24);
            Visible = true; // does not need to be discovered.
        }

        public override void HandlePickupQuery(Player player)
        {
            // prevent base.HandlePickupQuery() so you can't pick it up.
        }
    }
}
