﻿using Core.Actors.Pawns;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pickups
{
    public class AttackBoost : Pickup
    {
        /// <summary>
        /// Initializes a new instance of the AttackBoost class.
        /// </summary>
        public AttackBoost()
        {
            SpriteSource = new Rectangle(120, 0, 24, 24);
        }

        public override void HandlePickupQuery(Player player)
        {
            player.BaseDamage += player.Level;
            base.HandlePickupQuery(player);
        }
    }
}
