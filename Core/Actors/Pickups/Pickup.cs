﻿using Core.Actors.Pawns;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Core.Actors.Pickups
{
    public abstract class Pickup : Actor
    {
        public virtual void HandlePickupQuery(Player player)
        {
            DeleteMe = true;
        }

        public override void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("items");
            base.Load(content);
        }
    }
}
