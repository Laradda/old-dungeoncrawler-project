﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Core.Actors.Pickups
{
    public class HealthPotion : Pickup
    {
        public HealthPotion()
        {
            SpriteSource = new Rectangle(168,0,24,24);
        }

        public override void HandlePickupQuery(Pawns.Player player)
        {
            player.HealthPotions++;
            base.HandlePickupQuery(player);
        }

    }
}
