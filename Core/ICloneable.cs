﻿namespace Core
{
    /// <summary>
    /// Provides a method to clone itself to a specific type.
    /// </summary>
    /// <typeparam name="T">The type of the clone.</typeparam>
    public interface ICloneable<out T> where T : ICloneable<T>
    {
        /// <summary>
        /// Clones the current object.
        /// </summary>
        /// <returns>A cloned object</returns>
        T Clone();
    }
}
