﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class TickEventArgs : EventArgs
    {
        public TickType Type { get; private set; }
        public Effect Effect { get; private set; }
        public TickEventArgs(TickType type, Effect effect)
        {
            Type = type;
            Effect = effect;
        }
    }
}
