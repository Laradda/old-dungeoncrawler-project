﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class PoisonAttack : Effect<PoisonAttack>
    {
        public override PoisonAttack Combine(PoisonAttack effectA, PoisonAttack effectB)
        {
            return effectA.Clone();
        }

   
        public override string Description
        {
            get { return string.Format("Poison attack"); }
        }
    }
}
