﻿namespace Core.Effects
{
    public class DamageBonusFirstStrike : Effect<DamageBonusFirstStrike>
    {
        /// <summary>
        /// The percentage of damange bonus.
        /// </summary>
        public int Amount { get; set; }

        public override DamageBonusFirstStrike Combine(DamageBonusFirstStrike effectA, DamageBonusFirstStrike effectB)
        {
            DamageBonusFirstStrike clone = effectA.Clone();
            clone.Amount += effectB.Amount;
            return clone;
        }


        public override string Description
        {
            get { return string.Format("{0}% first strike damage bonus.", Amount); }
        }
    }
}
