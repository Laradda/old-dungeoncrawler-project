﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class ManaRecoverBonus : Effect<ManaRecoverBonus>
    {
        public int Amount { get; set; }

        public override ManaRecoverBonus Combine(ManaRecoverBonus effectA, ManaRecoverBonus effectB)
        {
            ManaRecoverBonus hrb = effectA.Clone();
            hrb.Amount += effectB.Amount;
            return hrb;
        }

        public override string Description
        {
            get { return string.Format("{0}% extra mana regeneration", Amount); }
        }


    }
}
