﻿namespace Core.Effects
{
    public class MagicResistance : Effect<MagicResistance>
    {
        /// <summary>
        /// The amount of damage resisted from a magic attack.
        /// </summary>
        public int Amount { get; set; }

        public override MagicResistance Combine(MagicResistance effectA, MagicResistance effectB)
        {
            MagicResistance clone = effectA.Clone();
            clone.Amount += effectB.Amount;
            return clone;

        }

  
        public override string Description
        {
            get { return string.Format("{0}% Magic resist",Amount); }
        }
    }
}
