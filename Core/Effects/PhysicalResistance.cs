﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class PhysicalResistance : Effect<PhysicalResistance>
    {
        /// <summary>
        /// The amount of damage resisted from a physical attack.
        /// </summary>
        public int Amount { get; set; }

        public override PhysicalResistance Combine(PhysicalResistance effectA, PhysicalResistance effectB)
        {
            PhysicalResistance clone = effectA.Clone();
            clone.Amount += effectB.Amount;
            return clone;

        }

    
        public override string Description
        {
            get { return string.Format("{0}% physical resist",Amount); }
        }
    }
}
