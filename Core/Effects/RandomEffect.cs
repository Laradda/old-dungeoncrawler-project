﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    /// <summary>
    /// Describes an effect that contains randomness.
    /// </summary>
    /// <typeparam name="TEffect"></typeparam>
    public abstract class RandomEffect<TEffect> : Effect<TEffect> where TEffect : Effect<TEffect>, new()
    {
        protected Random Rand = new Random();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chance">The chance this method will return true, between 0 and 100.</param>
        /// <returns></returns>
        public bool Roll(int chance)
        {
            return Rand.Next(1, 101) > chance;
        }
    }
}
