﻿namespace Core.Effects
{
    public class DamageBonus : Effect<DamageBonus>
    {
        /// <summary>
        /// The percentage of damange bonus.
        /// </summary>
        public int Amount { get; set; }

        public override DamageBonus Combine(DamageBonus effectA, DamageBonus effectB)
        {
            DamageBonus clone = effectA.Clone();
            clone.Amount += effectB.Amount;
            return clone;
        }


        public override string Description
        {
            get { return string.Format("{0}% damage bonus",Amount ); }
        }
    }
}
