﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class ManaBurn : Effect<ManaBurn>
    {
        public override ManaBurn Combine(ManaBurn effectA, ManaBurn effectB)
        {
            return effectA.Clone();
        }


        public override string Description
        {
            get { return string.Format("Mana burned"); }
        }
    }
}
