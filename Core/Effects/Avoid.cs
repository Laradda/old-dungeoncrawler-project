﻿using System;
using Core.Actors.Pawns;

namespace Core.Effects
{
    public class Avoid : RandomEffect<Avoid>
    {
        public event PlayerEventHandler Avoided;

        

        public Avoid()
        {
        }

        public void OnAvoided(Player player, EventArgs e)
        {
            if (Avoided != null) Avoided(player, e);
        }

        /// <summary>
        /// The change to avoid the attack.
        /// </summary>
        public int Chance{ get; set; }

        public override Avoid Combine(Avoid effectA, Avoid effectB)
        {
            Avoid clone = effectA.Clone();
            clone.Chance += effectB.Chance;
            return clone;
        }

        public override string Description
        {
            get { return string.Format("{0}% chance to avoid an attack.", Chance); }
        }
    }
}
