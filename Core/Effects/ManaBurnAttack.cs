﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class ManaBurnAttack : Effect<ManaBurnAttack>
    {
        public override ManaBurnAttack Combine(ManaBurnAttack effectA, ManaBurnAttack effectB)
        {
            return effectA.Clone();
        }


        public override string Description
        {
            get { return string.Format("Mana burn"); }
        }
    }
}
