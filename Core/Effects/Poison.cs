﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class Poison :Effect<Poison>
    {
        public override Poison Combine(Poison effectA, Poison effectB)
        {
            return effectA.Clone();
        }

    
        public override string Description
        {
            get { return string.Format("Poisoned"); }
        }
    }
}
