﻿using System;
using Core.Actors.Pawns;

namespace Core.Effects
{
    public abstract class LevelUpEffect<TEffect> : Effect<TEffect> where TEffect : Effect<TEffect>, new()
    {
        /// <summary>
        /// Raised when the player levels up.
        /// </summary>
        public event PlayerEventHandler LevelUp;

        public void OnLevelUp(Player player,EventArgs e)
        {
            if (LevelUp != null) LevelUp(player, e);
        }
    }
}
