﻿using System;

namespace Core.Effects
{
    public class LevelUpHealthBonus : LevelUpEffect<LevelUpHealthBonus>
    {
        /// <summary>
        /// The extra health gained when leveled up.
        /// </summary>
        public int HealthBonus{get; set; }

        public override LevelUpHealthBonus Combine(LevelUpHealthBonus effectA, LevelUpHealthBonus effectB)
        {
            throw new NotImplementedException();
        }

        public override LevelUpHealthBonus Clone()
        {
            throw new NotImplementedException();
        }

        public override string Description
        {
            get { throw new NotImplementedException(); }
        }
    }
}
