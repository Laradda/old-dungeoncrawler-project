﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
    public class HealthRecoverBonus : Effect<HealthRecoverBonus>
    {
        public int Amount { get; set; }

        public override HealthRecoverBonus Combine(HealthRecoverBonus effectA, HealthRecoverBonus effectB)
        {
            HealthRecoverBonus hrb = effectA.Clone();
            hrb.Amount += effectB.Amount;
            return hrb;
        }

        public override string Description
        {
            get { return string.Format("{0}% extra health regeneration", Amount); }
        }
    }
}
