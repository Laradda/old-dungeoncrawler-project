﻿namespace Core.Effects
{
    public enum TickType
    {
        DamageDone,
        DamageTaken
    }
}
