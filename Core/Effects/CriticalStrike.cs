﻿namespace Core.Effects
{
    public class CriticalStrike : RandomEffect<CriticalStrike>
    {
        /// <summary>
        /// The chance to critical strike.
        /// </summary>
        public int Chance { get; set; }

        public CriticalStrike()
        {
            Name = "Critical";
        }

        public override CriticalStrike Combine(CriticalStrike effectA, CriticalStrike effectB)
        {
            CriticalStrike clone = effectA.Clone();
            clone.Chance += effectB.Chance;
            return clone;
        }



        public override string Description
        {
            get { return string.Format("{0}% chance to cause double damage.", Chance); }
        }
    }
}
