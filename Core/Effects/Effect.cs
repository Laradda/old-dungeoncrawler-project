﻿using System;
using Core.Actors.Pawns;

namespace Core.Effects
{
    public abstract class Effect
    {
        /// <summary>
        /// The user-friendly name of the effect.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Wether this effect expires after an x amount of ticks.
        /// </summary>
        public bool Expires { get; set; }

        /// <summary>
        /// Describes the consequence of the effect.
        /// </summary>
        public abstract string Description { get; }

        /// <summary>
        /// The duration of the effect.
        /// </summary>
        public int Duration { get; set; }

        public event PlayerEventHandler<TickEventArgs> Tick;

        /// <summary>
        /// The type that created this effect.
        /// </summary>
        public Type Factory { get; set; }

        internal void OnTick(Player sender,TickEventArgs e)
        {
            if (Tick != null) Tick(sender, e);
        }
    }
    public abstract class Effect<TEffect> : Effect, ICloneable<TEffect> where TEffect : Effect<TEffect>, new ( )
    {

        /// <summary>
        /// Combines all the effects of the single effect derives type into a single effect.
        /// </summary>
        /// <param name="effectA"></param>
        /// <param name="effectB"></param>
        /// <returns></returns>
        public abstract TEffect Combine(TEffect effectA, TEffect effectB);

        /// <summary>
        /// Creates a clone of the current effect type.
        /// </summary>
        /// <returns></returns>
        public virtual TEffect Clone()
        {
            return (TEffect) MemberwiseClone();
        }
    }
}
