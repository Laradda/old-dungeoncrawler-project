﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Effects
{
   public class FirstStrike : Effect<FirstStrike>
    {
        public override FirstStrike Combine(FirstStrike effectA, FirstStrike effectB)
        {
            return effectA.Clone();
        }

        public override FirstStrike Clone()
        {
            FirstStrike clone = base.Clone();
            clone.Factory = Factory;
            return clone;
        }
        public override string Description
        {
            get { return string.Format("First strike"); }
        }
    }
}
