﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Actors;
using Core.Actors.Pawns;
using Core.Actors.Pawns.Creeps;
using Core.Actors.Pickups;
using Core.UI;
using DungeonCrawler.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Core.Actors.Tiles;

namespace Core
{
    public class Dungeon : DrawableGameComponent
    {
        public readonly GameState GameState;
        public readonly Camera Camera;
        public Interface UserInterface { get; private set; }

        public readonly Pathing PathFinder;
        public event EventHandler Victory;
        public event EventHandler Defeat;

        public MouseState CurrentMouse { private set; get; }
        public MouseState PreviousMouse { private set; get; }

        public Dungeon(Game game, GameState gameState, Camera camera) : base(game)
        {
            GameState = gameState;
            PathFinder = new Pathing(GameState);
            Camera = camera;
        }

        public override void Initialize()
        {
            foreach (Tile a in GameState.Tiles)
                a.Init();
            foreach (Actor a in GameState.Objects.Values)
                a.Init();
            GameState.Player.Init();

            GameState.Player.Walk += delegate(Player p, StepEventArgs args)
            {

                // check if tile in bounds
                if (GameState.GetTile(args.Target) == null) return;
                // check if the tile is an obstacle.
                if (GameState.GetTile(args.Target).Obstacle) return;

                // check if there is an object on the tile that is also an obstacle.
                if (GameState.Objects.ContainsKey(args.Target))
                {
                    if (GameState.Objects[args.Target].Obstacle)
                    {
                        Actor a = GameState.Objects[args.Target];
                        a.OnCollide(p,EventArgs.Empty);
                        return;
                    }
                    if (GameState.Objects[args.Target] is Pickup)
                    {
                        ((Pickup)GameState.Objects[args.Target]).HandlePickupQuery(p);
                    }

                }
                p.OnStep(args);
            };
            foreach (Tile t in GameState.Tiles)
            {
                t.Found += delegate(Player sender, EventArgs args)
                {
                    sender.Regenerate();
                    foreach (Creep creep in GameState.Objects.Values.OfType<Creep>())
                    {
                        creep.Regenerate();
                    }
                };
            }
            foreach(Imp imp in GameState.Objects.Values.OfType<Imp>())
            {
                imp.Hit += delegate{
                    
                };
            }
            GameState.Player.Step += delegate(Player sender, StepEventArgs args)
            {
                Point[] discover = new Point[9]
                {
                    new Point(args.Target.X, args.Target.Y),
                    new Point(args.Target.X, args.Target.Y+1), 
                    new Point(args.Target.X+1, args.Target.Y), 
                    new Point(args.Target.X+1, args.Target.Y+1), 
                    new Point(args.Target.X-1, args.Target.Y), 
                    new Point(args.Target.X, args.Target.Y-1), 
                    new Point(args.Target.X-1, args.Target.Y-1), 
                    new Point(args.Target.X+1, args.Target.Y-1), 
                    new Point(args.Target.X-1, args.Target.Y+1)
                };
                foreach (Point p in discover)
                {
                    Tile t = GameState.GetTile(p);
                    if (t == null) continue;

                    // discover tiles around the new players position.
                    t.OnFound(sender, EventArgs.Empty);
                    // discover the object on that tile if exists.
                    if (GameState.Objects.ContainsKey(p))
                    {
                        GameState.Objects[p].OnFound(sender, EventArgs.Empty);
                    }
                }


            };
            // Step to self to discover tiles.
            GameState.Player.OnStep(new StepEventArgs(GameState.Player.Position));

            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            foreach (Tile a in GameState.Tiles)
                a.Load(Game.Content);
            foreach (Actor a in GameState.Objects.Values)
                a.Load(Game.Content);
            GameState.Player.Load(Game.Content);

            UserInterface = new Interface(GameState, GraphicsDevice);
            UserInterface.Load(Game.Content);
           

           
        }

        public override void Update(GameTime gameTime)
        {
            // update Mouse states
            PreviousMouse = CurrentMouse;
            CurrentMouse = Camera.GetMouseState();


            if (!GameState.Defeated)
                UpdateGameState();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            SpriteBatch sb = new SpriteBatch(GraphicsDevice);

            // draw tiles
            sb.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp,
                     DepthStencilState.Default, RasterizerState.CullNone, null, Camera.MapMatrix);
            UserInterface.DrawBackGround(sb);

            foreach (Tile a in GameState.Tiles)
            {
                if (a.Visible)
                    a.Draw(sb);
            }

            foreach (Actor a in GameState.Objects.Values)
            {
                if (a.Visible)
                    a.Draw(sb);
            }

            GameState.Player.Draw(sb);

            UserInterface.Draw(sb);

            sb.End();

            base.Draw(gameTime);
        }

        protected override void Dispose(bool disposing)
        {
            GameState.Dispose();
            
            base.Dispose(disposing);
        }

        protected virtual void UpdateGameState()
        {
            // Check if we're dead.
            if (GameState.Player.CurrentHealth < 1)
            {
                GameState.Defeated = true;
                if(Defeat != null)
                Defeat(this, EventArgs.Empty);
            }
            Point p = new Point((CurrentMouse.X - 8) / 24, (CurrentMouse.Y - 8) / 24);

            if (GameState.MouseTarget != null)
            {
                GameState.MouseTarget.Hover = false;
                GameState.MouseTarget.Color = Color.White;
            }

            GameState.MouseTarget = null;
            if (GameState.Objects.ContainsKey(p))
            {
                Actor o = GameState.Objects[p];
                if (o.Visible)
                {
                    GameState.MouseTarget = o;
                    o.Hover = true;

                    // casting spell?
                    if(GameState.Player.ActiveSpell != null && o is Creep)
                    {
                        o.Color = Color.Red;

                        // clicked
                        if(CurrentMouse.LeftButton == ButtonState.Released && PreviousMouse.LeftButton == ButtonState.Pressed)
                        {
                            GameState.Player.Cast(GameState.Player.ActiveSpell, o);
                        }
                    }
                }
            }

            UserInterface.Update(Camera);

            if (CurrentMouse.LeftButton == ButtonState.Released && PreviousMouse.LeftButton == ButtonState.Pressed)
            {
                Stack<Point> way = PathFinder.FindPath(p);
                if (way != null)
                    GameState.Player.OnWalk(new StepEventArgs(p));
            }

            // covert dead creep into blood piles
            List<Point> corpses = (from a in GameState.Objects.Values.OfType<Creep>() where a.Dead select a.Position).ToList();
            foreach (Point point in corpses)
            {
                // remove the dead creep
                GameState.Objects.Remove(point); 
                // add blood pile
                BloodPile bloodPile = new BloodPile { Position = point };
                bloodPile.Load(Game.Content);
                GameState.Objects[point] = bloodPile;

            }


            // remove pickup up items
            List<Point> removing = (from a in GameState.Objects.Values where a.DeleteMe select a.Position).ToList();
            foreach (Point point in removing)
            {
                GameState.Objects.Remove(point);
            }

            GameState.Player.Update();

        }


    }
}