﻿using System;
using Core.Actors;
using Core.Actors.Pawns;
using Core.Actors.Pawns.Creeps;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Spells
{
    public class Drain : Spell
    {


        public Drain()
        {
            Name = "Drain life";
            Description = "Drains a target of life, healing yourself.";
            ManaCost = 5;
            SpriteSource = new Rectangle(392, 0, 56, 56);
        }

        public override bool Cast(Player caster, Actor target)
        {

            if (!(target is Creep))
                return false;

            Creep creep = (Creep)target;

            int damage = caster.Level * 2;

            MagicResistance mr = creep.EffectManager.GetEffect<MagicResistance>();

            if (mr != null)
                damage -= damage * (mr.Amount / 100);

            if (damage < 0)
                damage = 0;

            creep.DealDamage(damage, caster);

            caster.DealDamage(-damage);


            return true;
        }


    }
}
