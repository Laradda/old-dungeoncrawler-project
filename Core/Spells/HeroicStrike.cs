﻿using System.Linq;
using Core.Actors;
using Core.Actors.Pawns;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Spells
{
    public class HeroicStrike : Spell 
    {
        public HeroicStrike()
        {
            ManaCost = 4;
            SelfCast = true;
            SpriteSource = new Rectangle(0,0,56,56);
        }
    


        public override bool Cast(Player caster, Actor target)
        {
            if (!(target is Player))
                return false;

            Player t = (Player) target;


            // Already a spell like this. We may only apply 1 this way.
            if (caster.EffectManager.Effects.Any(effect => effect.Factory == GetType()))
                return false;
            

            DamageBonus db = new DamageBonus{
                Amount = 30,
                Duration = 1,
                Name = "Heroic Strike",
                Expires = true,
                Factory = GetType()
            };
            db.Tick += delegate(Player sender, TickEventArgs args){
                if(args.Type == TickType.DamageDone)
                {
                    db.Duration--;
                }
            };

            t.EffectManager.Effects.Add(db);

            return true;
        }


    }
}
