﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Actors;
using Core.Actors.Pawns;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Spells
{
    public class Block :Spell
    {
        public Block()
        {
            Name = "Block";
            Description = "Raise your shield preparing for an incoming attack.";
            ManaCost = 7;
            SpriteSource = new Rectangle(168, 0, 56, 56);
            SelfCast = true;
        }

        public override bool Cast(Player caster, Actor target)
        {
            if (!(target is Player))
                return false;
            Player t = (Player)target;


            // Already a spell like this. We may only apply 1 this way.
            if (caster.EffectManager.Effects.Any(effect => effect.Factory == GetType()))
                return false;


            PhysicalResistance db = new PhysicalResistance
            {
                Amount = 50,
                Duration = 1,
                Name = "Shield raised",
                Expires = true,
                Factory = GetType()
            };
            db.Tick += delegate(Player sender, TickEventArgs args)
            {
                if (args.Type == TickType.DamageTaken)
                {
                    db.Duration--;
                }
            };

            t.EffectManager.Effects.Add(db);

            return true;

        }
    }
}
