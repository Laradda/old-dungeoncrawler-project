﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Actors;
using Core.Actors.Pawns;
using Core.Actors.Pawns.Creeps;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Spells
{
   public class Shoot : Spell
    {
        public Shoot()
        {
            Name = "Shoot";
            Description = "Shoot with arrow and bow.";
            ManaCost = 6;
            SpriteSource = new Rectangle(112, 0, 56, 56);
        }

        public override bool Cast(Player caster, Actor target)
        {
            
            if(!(target is Creep))
                return false;

            Creep creep = (Creep)target;

            int damage = caster.Level * 4;

            PhysicalResistance mr = creep.EffectManager.GetEffect<PhysicalResistance>();

            if (mr != null)
                damage -= damage * (mr.Amount / 100);


            if (damage < 0)
                damage = 0;

            creep.DealDamage(damage, caster);


            return true;


        }
    }
}
