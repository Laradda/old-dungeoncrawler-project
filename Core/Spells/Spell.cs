﻿using Core.Actors;
using Core.Actors.Pawns;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Core.Spells
{
   
    public abstract class Spell 
    {
        /// <summary>
        /// The user-friendly name of the spell
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The user-friendly description of the spell
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The amount of mana points neede to cast this spell.
        /// </summary>
        public int ManaCost { get; set; }

        public bool SelfCast { get; set; }
        public Texture2D SpriteSheet;
        public Rectangle? SpriteSource;

        public virtual void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("spells");
        }

        public abstract bool Cast(Player caster, Actor target);


    }
}
