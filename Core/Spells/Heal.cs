﻿
using Core.Actors;
using Core.Actors.Pawns;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Spells
{
    public class Heal : Spell
    {
        public Heal()
        {
            ManaCost = 5;
            SelfCast = true;
            SpriteSource = new Rectangle(224, 0, 56, 56);

        }

        public override bool Cast(Player caster, Actor target)
        {
            if (!(target is Player))
                return false;

            Player t = (Player)target;
            t.EffectManager.RemoveEffect<Poison>();
            t.DealDamage(-(caster.Level*4));

            return true;
        }
    }
}
