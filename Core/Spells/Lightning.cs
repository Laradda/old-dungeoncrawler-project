﻿using System;
using Core.Actors;
using Core.Actors.Pawns;
using Core.Actors.Pawns.Creeps;
using Core.Effects;
using Microsoft.Xna.Framework;

namespace Core.Spells
{
    public class Lightning : Spell
    {


        public Lightning()
        {
            Name = "Lightning";
            Description = "Strikes a target with lightning";
            ManaCost = 6;
            SpriteSource = new Rectangle(336, 0, 56, 56);
        }

        public override bool Cast(Player caster, Actor target)
        {

            if (!(target is Creep))
                return false;

            Creep creep = (Creep)target;

            int damage = caster.Level * 4;

            MagicResistance mr = creep.EffectManager.GetEffect<MagicResistance>();

            if (mr != null)
                damage -= damage * (mr.Amount / 100);

            if (damage < 0)
                damage = 0;

            creep.DealDamage(damage, caster);



            return true;
        }


    }
}
