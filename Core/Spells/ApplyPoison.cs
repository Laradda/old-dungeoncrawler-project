﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Actors;
using Core.Actors.Pawns;
using Microsoft.Xna.Framework;
using Core.Effects;

namespace Core.Spells
{
    public class ApplyPoison : Spell
    {
        public ApplyPoison()
        {
            ManaCost = 5;
            SelfCast = true;
            SpriteSource = new Rectangle(280, 0, 56, 56);

        }

        public override bool Cast(Player caster, Actor target)
        {
            if (!(target is Player))
                return false;

            Player t = (Player)target;

            if (t.EffectManager.GetEffect<PoisonAttack>() != null)
                return false;

            PoisonAttack db = new PoisonAttack
            {
                Duration = 1,
                Name = "Poison",
                Expires = true
            };
            db.Tick += delegate(Player sender, TickEventArgs args)
            {
                if (args.Type == TickType.DamageDone)
                {
                    db.Duration--;
                }
            };

            t.EffectManager.Effects.Add(db);

            return true;
        }


    }
}
