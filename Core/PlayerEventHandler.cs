﻿using System;
using Core.Actors.Pawns;

namespace Core
{
    public delegate void PlayerEventHandler(Player sender, EventArgs e);
    public delegate void PlayerEventHandler<in T>(Player sender, T e);
}
