namespace DungeonCrawler
{
#if WINDOWS || XBOX
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            using (DungeonCrawler game = new DungeonCrawler())
            {
                game.Run();
            }
        }
    }
#endif
}