﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonCrawler.Core
{
    /// <summary>
    /// Contains event data provided for a event in the dungeon.
    /// </summary>
    public class DungeonEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the GameState object for the current dungeon.
        /// </summary>
        public GameState GameState { get; private set; }

        /// <summary>
        /// Initializes a new instance of the DungeonEventArgs class specifying a GameState object.
        /// </summary>
        /// <param name="gameState">The GameState object for the current dungeon</param>
        public DungeonEventArgs(GameState gameState)
        {
            GameState = gameState;
        }

    }
}