﻿using System;
using System.Collections.Generic;
using DungeonCrawler.Engine;
using DungeonCrawler.Engine.Tile;
using Microsoft.Xna.Framework;

namespace DungeonCrawler.Core
{
    /// <summary>
    /// Contains all the needed in-game information.
    /// </summary>
    public class GameState
    {
        /// <summary>
        /// Gets the tiles in the map.
        /// </summary>
        public Tile[] Tiles { get; private set; }
        /// <summary>
        /// Gets a dictionary containing an actor on each specified point.
        /// </summary>
        public Dictionary<Point, Actor> Objects { get; private set; }
        /// <summary>
        /// Gets the player object.
        /// </summary>
        public Player Player { get; private set; }
        /// <summary>
        /// Gets the size of the map.
        /// </summary>
        public Point MapSize { get; private set; }
        /// <summary>
        /// Gets a value indicating wether the player has been defeated.
        /// </summary>
        public bool Defeated { get; set; }
        /// <summary>
        /// Get the actor that is currently targeted with the mouse.
        /// </summary>
        public Actor MouseTarget;

        /// <summary>
        /// Initializes a new instance of the GameState class specifying the size of the Game, an array of tiles and dictionary of point, actor objects.
        /// </summary>
        /// <param name="size">The size of the Game</param>
        /// <param name="tiles">An array of tiles matching the size of the game ((width+1)*(height+1))</param>
        /// <param name="objects">Dictionary of point, actor objects.</param>
        public GameState(Point size, Tile[] tiles, Dictionary<Point, Actor> objects)
        {
            Tiles = tiles;
            Objects = objects;
            Player = new Player();
            MapSize = size;
        }

        /// <summary>
        /// Gets a tile from the tile array specifying a coordinate.
        /// </summary>
        /// <param name="x">The x-coordinate of the tile.</param>
        /// <param name="y">The y-coordinate of the tile.</param>
        /// <returns>The tile on the specified coordinate or null if the coordinates are out of bound</returns>
        public Tile GetTile(int x, int y)
        {
            if (x > MapSize.X || y > MapSize.Y || x < 0 || y < 0)
                return null;
            return Tiles[x + y*MapSize.X];
        }

        /// <summary>
        /// Gets a tile from the tile array specifying a coordinate.
        /// </summary>
        /// <param name="point">The point containing the x and y coordinate.</param>
        /// <returns>The tile on the specified coordinate or null if the coordinates are out of bound</returns>
        public Tile GetTile(Point point)
        {
            if (point.X > MapSize.X || point.Y > MapSize.Y || point.X < 0 || point.Y < 0)
                return null;
            return Tiles[point.X + point.Y*(MapSize.X + 1)];
        }
    }
}