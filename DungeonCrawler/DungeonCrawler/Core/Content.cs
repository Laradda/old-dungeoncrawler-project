﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Core
{
    public class Content
    {
        public Texture2D bg;
        public Texture2D creatures;
        public Texture2D items;
        public Texture2D portraits;
        public Texture2D tiles;
        public SpriteFont Font8;
        public SpriteFont Font10;
        public SpriteFont Font14;

        public void Load(ContentManager content)
        {
        }
    }
}
