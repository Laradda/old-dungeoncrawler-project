﻿using System.Collections.Generic;
using DungeonCrawler.Engine.Tile;
using Microsoft.Xna.Framework;

namespace DungeonCrawler.Core
{
   public class Pathing
   {
       protected GameState GameState;

       public Pathing(GameState gameState)
       {
           GameState = gameState;
       }

       /// <summary>
       /// Find a path from the players position to the specified destination point.
       /// </summary>
       /// <param name="dest"></param>
       /// <returns></returns>
       public Stack<Point> FindPath(Point dest)
       {
           Point start = GameState.Player.Position;

           // already at destination
           if (start == dest)
               return null;

           // out of bounds.
           if (GameState.GetTile(dest) == null)
               return null;

           // target is wall or an obstacle? fast return.
           if (GameState.GetTile(dest).Obstacle ||
               (GameState.Objects.ContainsKey(dest) && GameState.Objects[dest].Obstacle))
               return null;

           // target not discovered
           if (!GameState.GetTile(dest).Visible)
               return null;

           int depth = 0; // used in the pathmap to mark the number of steps to the destination.
           Stack<Point> waypoints = new Stack<Point>();
           Dictionary<Point, int> pathMap = new Dictionary<Point, int>();
           Point[] current = new Point[1] {dest};

           while (true)
           {
               List<Point> found = new List<Point>();
               foreach (Point p in current)
               {
                   if (p == start)
                   {
                       #region path found -> find shortest

                       Point pos = p;
                       while (true)
                       {
                           Point up = new Point(pos.X, pos.Y - 1),
                                 down = new Point(pos.X, pos.Y + 1),
                                 left = new Point(pos.X - 1, pos.Y),
                                 right = new Point(pos.X + 1, pos.Y);

                           Dictionary<Point, int> validPoints = new Dictionary<Point, int>();

                           if (pathMap.ContainsKey(up))
                               validPoints[up] = pathMap[up];
                           if (pathMap.ContainsKey(down))
                               validPoints[down] = pathMap[down];
                           if (pathMap.ContainsKey(left))
                               validPoints[left] = pathMap[left];
                           if (pathMap.ContainsKey(right))
                               validPoints[right] = pathMap[right];

                           Point step = default(Point);
                           int len = -1;
                           foreach (KeyValuePair<Point, int> d in validPoints)
                           {
                               if (len == -1 || d.Value < len)
                               {
                                   len = d.Value;
                                   step = d.Key;
                                   if (len == 0)
                                       break;
                               }
                           }
                           pos = step;
                           waypoints.Push(step);

                           if (step == dest)
                           {
                               waypoints.Push(dest);
                               Stack<Point> reversed = new Stack<Point>();
                               while (waypoints.Count > 0)
                                   reversed.Push(waypoints.Pop());

                               return reversed;
                           }
                       }

                       #endregion
                   }
                   else
                   {
                       pathMap[p] = depth;

                       Point[] points = new Point[4]
                                            {
                                                new Point(p.X, p.Y - 1),
                                                new Point(p.X + 1, p.Y),
                                                new Point(p.X, p.Y + 1),
                                                new Point(p.X - 1, p.Y)
                                            };
                       foreach (Point pt in points)
                       {
                           if (pathMap.ContainsKey(pt))
                               continue;

                           Tile t = GameState.GetTile(pt);

                           if (t != null && !t.Obstacle && t.Visible)
                           {
                               if (!GameState.Objects.ContainsKey(pt) || !GameState.Objects[pt].Obstacle)
                                   found.Add(pt);
                           }

                       }


                   }
               }
               if (found.Count == 0)
                   return null;

               current = found.ToArray();
               depth++;
           }
       }
   }
}
