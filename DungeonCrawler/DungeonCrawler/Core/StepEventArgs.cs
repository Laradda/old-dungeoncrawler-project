﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonCrawler.Core
{
    public class StepEventArgs : EventArgs
    {
        public Point Target { get; private set; }

        public StepEventArgs(Point target)
        {
            Target = target;
        }
    }
}
