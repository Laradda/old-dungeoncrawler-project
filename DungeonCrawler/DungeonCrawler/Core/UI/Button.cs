﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DungeonCrawler.Core.UI
{
    public class Button
    {
        protected MouseState PreviousMouseState;
        protected MouseState CurrentMouseState;

        public event EventHandler Clicked;
        public event EventHandler MouseOver;
        public int Width { get; set; }
        public int Height { get; set; }
        public Vector2 Position { get; set; }
        public string Text { get; set; }
        public bool IsMouseOver { get; protected set; }

        public void Load(ContentManager content)
        {
        }

        public void Update( Camera camera )
        {
            MouseState ms = camera.GetMouseState();
            PreviousMouseState = CurrentMouseState;
            CurrentMouseState = ms;

            if(ms.X > Position.X && ms.Y > Position.Y && ms.X < Position.X+Width && ms.Y < Position.Y + Height)
            {
                if(!IsMouseOver)
                {
                    IsMouseOver = true;
                    if (MouseOver != null)
                        MouseOver(this, EventArgs.Empty);
                }

                if(ms.LeftButton == ButtonState.Released && PreviousMouseState.LeftButton == ButtonState.Pressed)
                {
                    if (Clicked != null)
                        Clicked(this, EventArgs.Empty);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
        }
    }
}
