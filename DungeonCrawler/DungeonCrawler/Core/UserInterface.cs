﻿using System;
using DungeonCrawler.Engine.Creep;
using DungeonCrawler.Engine.Structure;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Core
{
    public class UserInterface 
    {

        protected SpriteFont Font8;
        protected SpriteFont Font10;
        protected SpriteFont Font14;

        protected GameState GameState;

        protected Texture2D Portraits;
        protected Texture2D Background;
        protected Texture2D Items;

        protected Vector2 UiPosition;

        public UserInterface(GameState gameState)
        {
            GameState = gameState;
            UiPosition = new Vector2(23 * 24 + 16 + 8, 8);
        }
        public virtual void Load(ContentManager content)
        {
            Font8 = content.Load<SpriteFont>("Font8");
            Font10 = content.Load<SpriteFont>("Font10");
            Font14 = content.Load<SpriteFont>("Font14");
            
            Portraits = content.Load<Texture2D>("portraits");
            Background = content.Load<Texture2D>("bg");
            Items = content.Load<Texture2D>("items");
        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            // draw portrait
            spriteBatch.Draw(Portraits, UiPosition, GameState.Player.PortraitSource, Color.White);
            // draw level + title
            spriteBatch.DrawString(Font14,String.Format("Level {0} {1}", GameState.Player.Level,GameState.Player.Name), 
                UiPosition+new Vector2(80,0),Color.White);
            
            // gold stat
            spriteBatch.Draw(Items, UiPosition + new Vector2(80, 24),new Rectangle(24,0,24,24), Color.White);
            spriteBatch.DrawString(Font10, GameState.Player.Gold.ToString(), UiPosition + new Vector2(100, 28), Color.White);

            // attack stat
            spriteBatch.Draw(Items, UiPosition + new Vector2(80, 48), new Rectangle(48, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, GameState.Player.Attack.ToString(), UiPosition + new Vector2(100, 50), Color.White);

            // health bar
            spriteBatch.Draw(GameState.Player.GetHealthBar(spriteBatch.GraphicsDevice), UiPosition + new Vector2(24, 80), Color.White);
            string health = String.Format("{0} / {1}", GameState.Player.Health, GameState.Player.MaxHealth);
            spriteBatch.DrawString(Font10, health, UiPosition + new Vector2(24 + (192 / 2 - Font10.MeasureString(health).X / 2), 82), Color.White);

            // xp bar
            spriteBatch.Draw(GameState.Player.GetXpBar(spriteBatch.GraphicsDevice), UiPosition + new Vector2(24, 108), Color.White);
            string xp = String.Format("{0} / {1}", GameState.Player.Xp, GameState.Player.XpToNextLevel);
            spriteBatch.DrawString(Font10, xp, UiPosition + new Vector2(24 + (192 / 2 - Font10.MeasureString(health).X / 2), 110), Color.White);

            // draw target
            if (GameState.MouseTarget != null && GameState.MouseTarget is Creep)
                DrawTarget(spriteBatch, (Creep)GameState.MouseTarget);
            // else structure we're standing on
            else if(GameState.Objects.ContainsKey(GameState.Player.Position) && GameState.Objects[GameState.Player.Position] is Structure)
                DrawStructure(spriteBatch, (Structure)GameState.Objects[GameState.Player.Position]);
        }
        public virtual void DrawBackGround(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Background, Vector2.Zero, Color.White);
        }
        public virtual void DrawTarget(SpriteBatch spriteBatch,Creep creep)
        {

            spriteBatch.Draw(Portraits,new Vector2(0,256)+UiPosition,creep.PortraitSource,Color.White );
            spriteBatch.DrawString(Font10,String.Format("Level {0} {1}",creep.Level,creep.Name), new Vector2(80,256)+UiPosition,Color.White);


            // attack stat
            spriteBatch.Draw(Items, UiPosition + new Vector2(80, 304), new Rectangle(48, 0, 24, 24), Color.White);
            spriteBatch.DrawString(Font10, creep.Attack.ToString(), UiPosition + new Vector2(100, 306), Color.White);

            spriteBatch.Draw(creep.GetHealthBar(spriteBatch.GraphicsDevice),UiPosition+new Vector2(24,336),Color.White);
            string health = String.Format("{0} / {1}", creep.Health, creep.MaxHealth);
                spriteBatch.DrawString(Font10, health, UiPosition + new Vector2(24 + (192 / 2 - Font10.MeasureString(health).X / 2), 338), Color.White);
            
        }
        public virtual void DrawStructure(SpriteBatch spriteBatch, Structure structure)
        {
            spriteBatch.DrawString(Font14, structure.Title, new Vector2(24, 256) + UiPosition, Color.White);
            int y = 40;
            foreach(string str in structure.Description)
            {
                spriteBatch.DrawString(Font10, str, new Vector2(24, 256+y) + UiPosition, Color.White);
                y += 16;
            }
        }
    }
}
