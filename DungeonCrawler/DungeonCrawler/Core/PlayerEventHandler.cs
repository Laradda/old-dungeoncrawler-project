﻿using DungeonCrawler.Engine;

namespace DungeonCrawler.Core
{
    /// <summary>
    /// Represents a method that will handle an event triggered by a player.
    /// </summary>
    /// <param name="sender">The player that triggers this event.</param>
    /// <param name="e">The EventArgs send with the event.</param>
    public delegate void PlayerEventHandler<in T>(Player sender, T e);
}