﻿using System;
using System.Collections.Generic;
using System.Linq;
using DungeonCrawler.Engine;
using DungeonCrawler.Engine.Creep;
using DungeonCrawler.Engine.Item;
using DungeonCrawler.Engine.Structure;
using DungeonCrawler.Engine.Tile;
using Microsoft.Xna.Framework;
using DungeonCrawler.Engine.Pickup;

namespace DungeonCrawler.Core
{
    public class DungeonGenerator
    {
        #region map variables

        /// <summary>
        /// zero-based width
        /// </summary>
        protected int Width;
        /// <summary>
        /// zero-based height
        /// </summary>
        protected int Height;

        protected int[] NumCreep = new int[]
        {
            10,10,10,7,6,5,4,4,3
        };

        protected int NumGoldPiles = 10;
        protected int NumHealthBoosts = 4;
        protected int NumAttackBoosts = 4;
        protected int NumShops = 4;

        private readonly List<Tile> _tiles = new List<Tile>();
        private readonly Player _player = new Player();
        private readonly List<Actor> _objects = new List<Actor>();

        #endregion

        #region generator fields.

        protected Random Rand = new Random();

        private readonly float[] Vars = new[] { .7f };
        private Stack<Point> empty;
        #endregion

        public DungeonGenerator(int width, int height)
        {
            Width = width;
            Height = height;
        }
        public GameState Generate()
        {

            int numTiles = 0;
            for (int i = 0; i <= 9; i += 2)
            {
                numTiles += AddSquare(new Point(i, i));
            }

            int tilesToRemove = (int) (numTiles*Vars[0]);

            // remove x% random tiles
            Tile[] removeTiles = _tiles.OrderBy(x => Rand.Next()).Take(tilesToRemove).ToArray();
            foreach (Tile t in removeTiles) _tiles.Remove(t);

            // walk squares to fill empty spaces with a long line.
            foreach (Tile t in _tiles.ToArray())
            {
                WalkSquare(t.Position);
            }
            FixLooseSquares();

            Point start = AddStartingArea();
            _player.Position = start;


            // done with walls
            empty = new Stack<Point>();
            // fill the rest with ground.
            foreach (Point p in GetEmptyPoints())
            {
                _tiles.Add(new Ground { Position = p });
                if(p != start)
                    empty.Push(p);
            }
            empty = new Stack<Point>(empty.OrderBy(x => Rand.Next()));

            AddCreep();
            AddItems();


            int total = (Width + 1)*(Height + 1);
            // wrap up
            Tile[] tiles = new Tile[total];

            for (int x = 0; x <= Width; x++)
            {
                for (int y = 0; y <= Height; y++)
                {
                    tiles[x + y*(Width + 1)] = GetTile(x, y);
                }
            }

            Dictionary<Point, Actor> objects = _objects.ToDictionary(o => o.Position);

            GameState newGame = new GameState(new Point(Width, Height), tiles, objects);
            newGame.Player.Position = _player.Position;

            return newGame;
        }
        private void AddItems()
        {
            for (int i = 0; i < NumGoldPiles; i++)
            {
                _objects.Add(new Gold{
                    Position = empty.Pop(), 
                    Value = Rand.Next(1, 4)
                });
            }
            for (int i = 0; i < NumAttackBoosts; i++)
                _objects.Add(new AttackBoost {Position = empty.Pop()});

            for (int i = 0; i < NumHealthBoosts; i++)
                _objects.Add(new HealthBoost { Position = empty.Pop() });
            for (int i = 0; i < NumHealthBoosts; i++)
                _objects.Add(new Shop(new SmallSword()) { Position = empty.Pop() });

        }

        private void AddCreep()
        {
            int level = 1;

            foreach(int amount in NumCreep)
            {

                for (int i = 0; i < amount; i++)
                {
                    Creep creep = GetRandomCreep();
                    creep.Position = empty.Pop();
                    creep.Level = level;
                    _objects.Add(creep);
                }


                level++;
            }


        }
        protected Creep GetRandomCreep()
        {
            return new Creep[]
                       {
                           new Beetle(),
                           new BileDemon(),
                        //   new Dragon(), 
                           new Fly(), 
                        //   new Ghost(), 
                        //   new HellHound(),
                        //   new HornedReaper(), 
                        //   new Skeleton(),
                           new Spider(), 
                        //   new Vampire(), 
                           new Wizard()
                       }.OrderBy(x => Rand.Next()).First();
        }
        private Point AddStartingArea()
        {
            const int fromEdge = 2,
                      fromCenter = 2;
            int x, y;
            // left or right? top of bottom?
            if (Rand.Next(0, 2) == 0)
            {
                x = Rand.Next(fromEdge, Width/2 - fromCenter);
            }
            else
            {
                x = Width - Rand.Next(fromEdge, Width / 2 - fromCenter);
            }

            if (Rand.Next(0, 2) == 0)
            {
                y = Rand.Next(fromEdge, Height / 2 - fromCenter);
            }
            else
            {
                y = Height - Rand.Next(fromEdge, Height / 2 - fromCenter);
            }

            RemoveWall(x,y);
            foreach(Point p in PointsNearPoint(new Point(x,y)))
                RemoveWall(p);

            return new Point(x,y);
        }
        private void FixLooseSquares()
        {
            // find loose (single) squares and add one next to them
            foreach (Point point in FindLooseSquares())
            {
                // points near the given point
                Point[] points = DirectPointsNearPoint(point);

                foreach (Point p in points.OrderBy(x => Rand.Next()))
                {
                    if (WallsNearWall(p) == 2 && WallsNearWallDirect(p) == 2)
                    {
                        SetWall(p);
                        break;
                    }
                }
            }
        }
        private void WalkSquare(Point point)
        {
            // points near the given point
            Point[] points = new Point[4]
            {
                new Point(point.X, point.Y + 1), 
                new Point(point.X, point.Y - 1), 
                new Point(point.X+1, point.Y ), 
                new Point(point.X-1, point.Y)
            };

            // randomize order
            points = points.OrderBy(x => Rand.Next()).ToArray();

            // target point
            foreach (Point p in points)
            {
                if (OutOfBounds(p))
                    continue;

                // points near source point
                List<Point> pointsNearPoint = FindWallsNearPoint(point);
                // points near target point
                List<Point> pointsNearP = FindWallsNearPoint(p);
                // number of points near target point - source point
                int otherSide = pointsNearP.Count -1;

                if (pointsNearP.Count >= 3)
                    continue;

                if(otherSide > 2)
                    continue;

                foreach(Point pt in pointsNearP)
                    if (pointsNearPoint.Contains(pt))
                        otherSide--;
                

                if(otherSide > 0)
                    continue;
                if(NearEdge(p))
                    continue;
                
                if(NearEdge(p) && NearEdge(point))
                    continue;

                if(SetWall(p)) // continue on that path 
                    WalkSquare(p);
            }
        }
        /// <summary>
        /// Adds an empty square of walls in the map, with a specified offset from the edge.
        /// </summary>
        /// <param name="offset">Offset from the edge</param>
        /// <returns>Number of walls added</returns>
        private int AddSquare(Point offset)
        {
            int num = 0;
            // line top
            for (int i = offset.X; i <= Width - offset.X; i++)
                if (SetWall(i, offset.Y)) num++;

            // line bottom
            for (int i = offset.X; i <= Width - offset.X; i++)
                if (SetWall(i, Height - offset.Y)) num++;

            // line left
            for (int i = offset.Y; i <= Height - offset.Y; i++)
                if (SetWall(offset.X, i)) num++;

            // line right
            for (int i = offset.Y; i <= Height - offset.Y; i++)
                if (SetWall(Width - offset.X, i)) num++;

            return num;
        }


        #region helper methods
        protected List<Point> GetEmptyPoints()
        {
            List<Point> points = new List<Point>();
            for (int x = 0; x <= Width; x++)
            {
                for (int y = 0; y <= Height; y++)
                {
                    if (!IsWall(x, y))
                        points.Add(new Point(x, y));
                }

            }
            return points;
        }

        protected List<Point> FindLooseSquares()
        {
            return (from point in _tiles where WallsNearWall(point.Position) == 0 select point.Position).ToList();
        }
        protected bool NearEdge(Point point)
        {
            return point.X == 0 || point.Y == 0 || point.X == Width || point.Y == Height;
        }
        protected bool NearEdge(int x, int y)
        {
            return NearEdge(new Point(x, y));
        }
        protected bool IsWall(Point point)
        {
            return _tiles.Any(a => a.Position == point);
        }
        protected bool IsWall(int x, int y)
        {
            return IsWall(new Point(x, y));
        }
        protected bool OutOfBounds(Point point)
        {
            return point.X < 0 || point.Y < 0 || point.X > Width || point.Y > Height;
        }
        protected bool OutOfBounds(int x, int y)
        {
            return OutOfBounds(new Point(x, y));
        }
        protected Tile GetTile(int x, int y)
        {foreach(Tile t in _tiles)
        {
            if (t.Position.X == x && t.Position.Y == y)
                return t;
        }
            throw new ArgumentOutOfRangeException();
        }

        protected bool SetWall(Point point)
        {
            if (IsWall(point))
                return false;

            if (OutOfBounds(point))
                return false;

            _tiles.Add(new Wall { Position = point });
            return true;
        }
        protected bool SetWall(Point point, Color color)
        {
            if (IsWall(point))
                return false;

            if (OutOfBounds(point))
                return false;

            _tiles.Add(new Wall { Position = point });
            return true;
        }
        protected bool SetWall(int x, int y)
        {
            return SetWall(new Point(x, y));
        }
        protected void RemoveWall(Point point)
        {
            foreach (Tile a in _tiles)
            {
                if (a.Position == point)
                {
                    _tiles.Remove(a);
                    return;
                }
            }
        }
        protected void RemoveWall(int x, int y)
        {
            RemoveWall(new Point(x, y));
        }
        protected int WallsNearWall(Point point)
        {
            return PointsNearPoint(point).Count(IsWall);
        }
        protected int WallsNearWall(int x, int y)
        {
            return WallsNearWall(new Point(x, y));
        }
        protected Point[] PointsNearPoint(Point point)
        {
            return new[]
            {
                new Point(point.X + 1, point.Y), 
                new Point(point.X, point.Y + 1), 
                new Point(point.X + 1, point.Y + 1), 
                new Point(point.X + 1, point.Y - 1), 
                new Point(point.X - 1, point.Y + 1), 
                new Point(point.X - 1, point.Y), 
                new Point(point.X, point.Y - 1), 
                new Point(point.X - 1, point.Y - 1)
            };
        }
        protected Point[] DirectPointsNearPoint(Point point)
        {
            return new[]
            {
                new Point(point.X + 1, point.Y), 
                new Point(point.X, point.Y + 1), 
                new Point(point.X - 1, point.Y), 
                new Point(point.X, point.Y - 1)
            };
        }
        protected List<Point> FindWallsNearPoint(Point point)
        {
            return PointsNearPoint(point).Where(IsWall).ToList();
        }
        protected List<Point> FindWallsNearPointDirect(Point point)
        {
            return DirectPointsNearPoint(point).Where(IsWall).ToList();
        }
        protected int WallsNearWallDirect(Point point)
        {
            return DirectPointsNearPoint(point).Count(IsWall);
        }
        protected int WallsNearWallDirect(int x, int y)
        {
            return WallsNearWallDirect(new Point(x, y));
        }

        #endregion

    }
}
