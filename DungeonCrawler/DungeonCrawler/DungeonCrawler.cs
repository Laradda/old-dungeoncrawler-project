using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Xml.Linq;
using Core;
using Core.Actors.Pawns;
using Core.Actors.Pawns.Heroes;
using Core.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DungeonCrawler
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class DungeonCrawler : Game
    {
        public GraphicsDeviceManager Graphics { get;private set; }

        public readonly Camera View = new Camera();
        public SpriteBatch SpriteBatch { get; private set; }
        public Dungeon Dungeon { get; private set; }
        public StatusBar Bar { get; private set; }


        public Button StartButton = new Button{Width = 150,Height = 20,Text = "Start", Position = new Vector2(64, 496-64)};
        public Dictionary<Button, Player> ClassButtons = new Dictionary<Button, Player>(13);

        public Player SelectedPlayer { get; set; }

        public DungeonCrawler()
        {
            InitializeGraphics();
            
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Window.AllowUserResizing = true;
            Window.ClientSizeChanged += delegate { View.Update(GraphicsDevice.Viewport); };
            
            //DungeonGenerator dg = new DungeonGenerator(22, 19, new Player());

            //Dungeon = new Dungeon(this, dg.Generate(), View);
            
            Components.Add(Dungeon);

        }


        protected override void Initialize()
        {
            View.Update(GraphicsDevice.Viewport);
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            Bar = new StatusBar(GraphicsDevice);
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            StartButton.Load(Content);
            StartButton.ButtonTexture = Bar.CreateStatusBar(Color.DarkGray, 150, 30, 100,Color.DarkGray,2);
            StartButton.ButtonHoverTexture = Bar.CreateStatusBar(new Color(160,160,160,255), 150, 30, 100, Color.DarkGray, 2);
            StartButton.Font = Content.Load<SpriteFont>("Font10");
            StartButton.Clicked += delegate
            {
                if(SelectedPlayer != null)
                {
                    DungeonGenerator dg = new DungeonGenerator(Camera.TilesX-1, Camera.TilesY-1, SelectedPlayer);
                    Dungeon = new Dungeon(this, dg.Generate(),View);
                    Components.Add(Dungeon);
                }
            };
            Player[] classes = GetClasses();

            foreach (Player p in classes)
                p.Load(Content);

            const int classPerRow = 10;
            for (int i = 0; i < 13;i++ )
            {
                Button b = new Button {
                    Width = 64,
                    Height = 64,
                    Position = new Vector2(
                        (i%classPerRow)*64 + 8 + (16*(i%classPerRow)),
                        (i/classPerRow)*64 + 8 + (16*(i/classPerRow)))
                };

                ClassButtons.Add(b, classes[i]);
                b.ButtonTexture = classes[i].PortraitSprite;
                b.SpriteSource = classes[i].PortraitSource;
                b.Clicked += delegate(object sender, EventArgs args)
                {
                    SelectedPlayer = ClassButtons[(Button)sender];
                };
            }

            

            base.LoadContent();
        }
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) Exit();

            if(Dungeon == null)
                UpdateStartupScreen();

            base.Update(gameTime);
        }
        protected void UpdateStartupScreen()
        {
            StartButton.Update(View);

            foreach (Button b in ClassButtons.Keys)
                b.Update(View);
        }


        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

       

            if(Dungeon == null)
                DrawStartupScreen();


            base.Draw(gameTime);
        }


        
        protected void DrawStartupScreen()
        {
            SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.PointClamp,
         DepthStencilState.Default, RasterizerState.CullNone, null, View.MapMatrix);


            foreach (Button b in ClassButtons.Keys)
                b.Draw(SpriteBatch);

            StartButton.Draw(SpriteBatch);

            if (SelectedPlayer != null)
                DrawSelectedClass(SpriteBatch, SelectedPlayer);

            SpriteBatch.End();

        }
        protected void DrawSelectedClass(SpriteBatch spriteBatch,Player player)
        {
            spriteBatch.Draw(player.PortraitSprite,new Vector2(460,200),player.PortraitSource,Color.White );
            spriteBatch.DrawString(Content.Load<SpriteFont>("Font14"), player.Name,  new Vector2(540, 200), Color.White);

        }
        protected void InitializeGraphics()
        {
            Graphics = new GraphicsDeviceManager(this);
            Graphics.PreferredBackBufferWidth = 816;
            Graphics.PreferredBackBufferHeight = 512;
            
            
        }
        protected Player[] GetClasses()
        {
            return new Player[]
            {
                new Wizard(),
                new Tunneler(),
                new Thief(), 
                new Samurai(), 
                new Priestess(),
                new MountainDwarf(), 
                new Monk(), 
                new Knight(), 
                new Giant(), 
                new Fairy(), 
                new Barbarian(), 
                new Avatar(), 
                new Archer()
            };
        }

    }
}
