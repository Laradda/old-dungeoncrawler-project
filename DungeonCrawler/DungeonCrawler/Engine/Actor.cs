﻿
using System;
using DungeonCrawler.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Engine
{
    /// <summary>
    /// The base class of every actor in the game.
    /// </summary>
    public abstract class Actor
    {
        
        /// <summary>
        /// Zero-based tile position on the map. Ranges from 0-Width, 0-Height
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// The texture sprite sheet.
        /// </summary>
        public Texture2D SpriteSheet { get; set; }
        
        public Rectangle TextureSource { get; set; }

        public bool Visible { get; protected set; }
        public bool Obstacle { get; protected set; }
        public bool RemoveMe { get; set; }
        
        /// <summary>
        /// Raised when this Actor is discovered.
        /// </summary>
        public event PlayerEventHandler<EventArgs> Found;

        public event PlayerEventHandler<EventArgs> Hit;

        protected SpriteFont Font8;
        protected SpriteFont Font14;

        protected Actor()
        {
        }

        public virtual void Init()
        {
        }

        public virtual void Load(ContentManager content)
        {
            Font8 = content.Load<SpriteFont>("Font8");
            Font14 = content.Load<SpriteFont>("Font14");
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(SpriteSheet, 
                new Rectangle(Position.X * Camera.TileSize+8, Position.Y * Camera.TileSize+8,Camera.TileSize,Camera.TileSize),
                TextureSource, Color.White);
        }

        public void Discover(Player player)
        {
            if (Visible) return;

            Visible = true;
            if (Found != null)
                Found(player, EventArgs.Empty);
        }

        /// <summary>
        /// When a player walks into this creep.
        /// </summary>
        /// <param name="player"></param>
        public void Collide(Player player)
        {
            if (Hit != null)
                Hit(player, EventArgs.Empty);
        }
    }
}
