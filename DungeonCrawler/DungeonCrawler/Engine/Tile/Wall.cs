﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Tile
{
    public class Wall : Tile
    {
        public Wall()
        {
            Obstacle = true;
            //TextureSource = new Rectangle(224, 0, 32, 32);
            TextureSource = new Rectangle(64,0,32,32);
        }
    }
}
