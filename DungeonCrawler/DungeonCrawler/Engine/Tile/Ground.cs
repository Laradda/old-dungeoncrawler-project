﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Tile
{
    public class Ground : Tile
    {
        public Ground()
        {
            TextureSource = new Rectangle(320, 0, 32, 32);
        }
    }
}
