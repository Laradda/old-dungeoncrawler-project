﻿using System;
using DungeonCrawler.Core;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Engine.Tile
{
    public abstract class Tile : Actor
    {

        protected Tile()
        {
            Found += TileFound;
        }

        private void TileFound(Player sender, EventArgs e)
        {
            sender.GerenerateHealth();

        }

        public override void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("tiles");
            base.Load(content);
        }

        
    }
}
