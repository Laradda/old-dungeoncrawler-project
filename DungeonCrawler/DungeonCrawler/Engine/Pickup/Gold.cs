﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Pickup
{
    public class Gold : Pickup
    {

        public int Value { get; set; }

        public Gold()
        {
            TextureSource = new Rectangle(0, 0, 24, 24);
           

        }

        public override void HandlePickupQuery(Player player)
        {
            player.Gold = player.Gold + Value;
            
            base.HandlePickupQuery(player);
        }
    }
}