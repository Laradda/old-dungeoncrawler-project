﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Pickup
{
    public class BloodPile : Pickup
    {

        public BloodPile()
        {
            TextureSource = new Rectangle(72, 0, 24, 24);
            Visible = true;
        }

        public override void HandlePickupQuery(Player player)
        {
            
        }
    }
}
