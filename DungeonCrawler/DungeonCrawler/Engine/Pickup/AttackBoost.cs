﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Pickup
{
    public class AttackBoost : Pickup
    {
        public AttackBoost()
        {
            TextureSource = new Rectangle(120, 0, 24, 24);

        }

        public override void HandlePickupQuery(Player player)
        {
            player.DamageMod += 10;
            base.HandlePickupQuery(player);
        }

    }
}
