﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Engine.Pickup
{
    /// <summary>
    /// Represends actors that can be picked up in the world.
    /// </summary>
    public abstract class Pickup : Actor
  {
   
      public virtual void HandlePickupQuery(Player player)
      {
          RemoveMe = true;
      }

      public override void Load(ContentManager content)
      {
          SpriteSheet = content.Load<Texture2D>("items");
          base.Load(content);
      }
  }
}
