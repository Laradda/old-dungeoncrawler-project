﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Pickup
{
    public class HealthBoost : Pickup
    {
        public HealthBoost()
        {
            TextureSource = new Rectangle(96, 0, 24, 24);

        }

        public override void HandlePickupQuery(Player player)
        {
            player.MaxHealth += player.Level;
            base.HandlePickupQuery(player);
        }

    }
}
