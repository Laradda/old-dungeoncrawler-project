﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonCrawler.Engine.Item
{
    /// <summary>
    /// Represends items sold by a shop.
    /// </summary>
    public abstract class Item
    {
        public int Price { get; protected set; }
        public string Name { get; protected set; }
        public string[] Description { get; protected set; }

        public abstract void PickupItem(Player player);

    }
}