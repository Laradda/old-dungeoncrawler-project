﻿namespace DungeonCrawler.Engine.Item
{
    public class SmallSword : Item
    {
        public SmallSword()
        {
            Name = "Small Sword";
            
            Description = new string[1]
            {
                "Increases base damage by +5."
            };
            Price = 5;
        }

        public override void PickupItem(Player player)
        {
            player.Attack += 5;

        }
    }
}