﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Engine
{
    public abstract class Pawn : Actor
    {
        private int _health;
        private int _maxHealth;
        private bool _redrawHealthBar;

        public int Level { get; set; }
        public Rectangle PortraitSource { get; protected set; }
        public string Name { get; protected set; }
        public int Health
        {
            get { return _health; }
            set
            {
                _health = value;
                _redrawHealthBar = true;
                if (_health <= 0)
                    Death = true;
            }
        }
        public int MaxHealth
        {
            get { return _maxHealth; }
            set
            {
                _maxHealth = value;
                _redrawHealthBar = true;
            }
        }
        public virtual int Attack { get; set; }
        public Texture2D HealthBar { get;  set; }

        /// <summary>
        /// A value indicating wether the pawn has been killed or not.
        /// </summary>
        public bool Death { get; set; }

        protected Pawn()
        {
            _redrawHealthBar = true;
        }
        /// <summary>
        /// Genererates health for the current pawn.
        /// </summary>
        public virtual void GerenerateHealth()
        {
            if (Health >= MaxHealth) return;

            Health += MaxHealth/10;
            Health = Math.Min(Health, MaxHealth);
        }

        public Texture2D GetHealthBar(GraphicsDevice device)
        {
            if (_redrawHealthBar)
            {
                float percentage = (float) Health/MaxHealth;
                const int barWidth = 192,
                          barHeight = 20;

                Texture2D texture2D = new Texture2D(device, barWidth, 20);

                Color[] pixels = new Color[barWidth*barHeight];
                for (int i = 0, total = barWidth*barHeight; i < total; i++)
                {
                    int y = i/barWidth,
                        x = i%barWidth;
                    float p = (float) x/barWidth;

                    // is pixel at border
                    if (y <= 1 || x <= 1 || x >= barWidth - 2 || y >= barHeight - 2)
                        pixels[i] = Color.DarkRed;
                    else
                        pixels[i] = p > percentage ? Color.Transparent : Color.Red;

                }
                texture2D.SetData(pixels);
                HealthBar = texture2D;
                _redrawHealthBar = false;
            }
            return HealthBar;
        }
    }
}