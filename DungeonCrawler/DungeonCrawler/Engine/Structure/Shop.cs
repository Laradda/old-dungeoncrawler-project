﻿namespace DungeonCrawler.Engine.Structure
{
    public class Shop : Structure
    {
        public Item.Item Item { get; protected set; }

        public Shop(Item.Item item)
        {
            Item = item;
        }

        public override string Title
        {
            get { return Item.Name; }
        }
        public override string[] Description
        {
            get
            {
                return Item.Description;
            }
        }
    }
}
