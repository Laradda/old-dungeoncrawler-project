﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Engine.Structure
{
    public abstract class Structure : Actor
    {
        public virtual string[] Description { get; set; }
        public virtual string Title { get; set; }

        public Structure()
        {
            TextureSource = new Rectangle(142,0,24,24);
        }

        public override void Load(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("items");
            base.Load(content);
        }
    }
}
