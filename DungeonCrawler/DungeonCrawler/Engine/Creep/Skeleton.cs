﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class Skeleton : Creep
    {
        public Skeleton()
        {
            TextureSource = new Rectangle(64, 0, 32, 32);
            PortraitSource = new Rectangle(0, 360, 72, 72); Name = "Skeleton";
        }




    }
}
