﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class Fly : Creep
    {
        public Fly()
        {
            TextureSource = new Rectangle(32, 64, 32, 32);
            PortraitSource = new Rectangle(288, 144, 72, 72); Name = "Fly";
        }


        public override void Init()
        {
            MaxHealth = 5 * Level;
            Attack = Level * 3;

            base.Init();
        }


    }
}
