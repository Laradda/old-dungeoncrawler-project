﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class Wizard : Creep
    {
        public Wizard()
        {
            TextureSource = new Rectangle(128, 0, 32, 32);
            PortraitSource = new Rectangle(0, 288, 72, 72); Name = "Wizard";
        }


        public override void Init()
        {
            MaxHealth = 7 * Level;
            Attack = Level * 5;

            base.Init();
        }

    }
}
