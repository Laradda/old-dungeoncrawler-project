﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class Beetle : Creep
    {

    


       public Beetle()
       {
           TextureSource = new Rectangle(32,0,32,32);
           PortraitSource = new Rectangle(288, 216, 72, 72); Name = "Beetle";
       }

       public override void Init()
       {
           MaxHealth = 6 * Level;
           Attack = Level * 3;

           base.Init();
       }


    }
}
