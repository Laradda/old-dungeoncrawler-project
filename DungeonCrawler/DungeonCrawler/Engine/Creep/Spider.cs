﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class Spider : Creep
    {
        public Spider()
        {
            TextureSource = new Rectangle(128, 64, 32, 32);
            PortraitSource = new Rectangle(288, 288, 72, 72); Name = "Spider";
        }


        public override void Init()
        {
            MaxHealth = 7 * Level;
            Attack = Level * 3;

            base.Init();
        }

    }
}
