﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class Ghost : Creep
    {
        public Ghost()
        {
            TextureSource = new Rectangle(160, 32, 32, 32);
            PortraitSource = new Rectangle(216, 144, 72, 72);
            Name = "Ghost";
        }




    }
}
