﻿using Microsoft.Xna.Framework;

namespace DungeonCrawler.Engine.Creep
{
    public class BileDemon : Creep
    {
        public BileDemon()
        {
            TextureSource = new Rectangle(64, 64, 32, 32);
            PortraitSource = new Rectangle(216, 216, 72, 72);
            Name = "Bile Demon";
        }



        public override void Init()
        {
            MaxHealth = 11 * Level;
            Attack = Level * 2 ;

            base.Init();
        }

    }
}
