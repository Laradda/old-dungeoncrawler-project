﻿using System;
using DungeonCrawler.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonCrawler.Engine.Creep
{
    public abstract class Creep : Pawn
    {
        protected Creep()
        {
            Obstacle = true;
            Hit += OnHit;
        }

        public override void Init()
        {
            Health = MaxHealth;
        }

        public override void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("creatures");
            base.Load(content);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            spriteBatch.DrawString(Font8, Level.ToString(),
                new Vector2(Position.X * Camera.TileSize + 1 + 8, Position.Y * Camera.TileSize + 11 + 8), Color.White);
        }

        protected virtual void OnHit(Player sender, EventArgs e)
        {
            if (FirstStrike(sender))
            {
                PlayerHitCreep(sender);
                if(!Death)
                    CreepHitPlayer(sender);
            }
            else
            {
                CreepHitPlayer(sender);
                if (!sender.Death)
                    PlayerHitCreep(sender);
            }
        }

        protected virtual void PlayerHitCreep(Player p)
        {
            Health = Health - p.Attack;
            if(Death)
                Die(p);
        }

        protected virtual void CreepHitPlayer(Player p)
        {
            p.Health = p.Health - Attack;
        }
        protected virtual bool FirstStrike(Player p)
        {
            return p.Level > Level;
        }

        protected virtual void Die(Player p)
        {
            p.Xp += p.CalculateXp(this);
        }
    }
}
