﻿using System;
using DungeonCrawler.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using DungeonCrawler.Engine.Creep;
namespace DungeonCrawler.Engine
{
    public class Player : Pawn
    {
        private int xp;
        private bool redrawXpBar = true;
        private Texture2D XpBar;

        public float DamageMod { get; set; }
        public override int Attack
        {
            get
            {
                return (int)(base.Attack+ base.Attack *(DamageMod/100));
            }
        }
        public int Gold { get; set; }
        public int Xp
        {
            get { return xp; }
            set
            {
                redrawXpBar = true;
                xp = value;
                if (xp >= XpToNextLevel)
                {
                    xp -= XpToNextLevel;
                    XpToNextLevel = CalculateXp(Level + 1);
                    if (LevelUp != null)
                        LevelUp(this, EventArgs.Empty);
                }
            }
        }

        public int XpToNextLevel { get;protected set; }

        protected KeyboardState CurrentKs;
        protected KeyboardState PreviousKs;

        public event PlayerEventHandler<EventArgs> LevelUp;

        /// <summary>
        /// Occurs when the player stepped in a specific direction.
        /// </summary>
        public event PlayerEventHandler<StepEventArgs> Step;

        /// <summary>
        /// Occurs when the player walks in a specific direction.
        /// </summary>
        public event PlayerEventHandler<StepEventArgs> Walk;
    
        public Player()
        {
            TextureSource = new Rectangle(64, 96, 32, 32);
            PortraitSource = new Rectangle(72, 0, 72, 72);
            Name = "Knight";
            Level = 1;

            Visible = true;
            Step += delegate(Player sender, StepEventArgs e) { Position = e.Target; };
            
            MaxHealth = 15;
            Health = MaxHealth;
            Attack = 5;
            XpToNextLevel = CalculateXp(Level);
            LevelUp += OnLevelUp;

        }

        protected virtual void OnLevelUp(object sender, EventArgs e)
        {
            Level++;
            MaxHealth += 10;
            Health = MaxHealth;
            Attack += 5;
        }

        public override void Load(ContentManager content)
        {
            SpriteSheet = content.Load<Texture2D>("creatures");

            base.Load(content);
        }

        public void Update()
        {
            PreviousKs = CurrentKs;
            CurrentKs = Keyboard.GetState();

            HandleMovement();
        }

        protected virtual int CalculateXp(int level)
        {
            return 5*level;
        }

        protected void HandleMovement()
        {
            if (Walk == null) return;

            Point targetStep = Position;

            if (PreviousKs.IsKeyUp(Keys.Left) && CurrentKs.IsKeyDown(Keys.Left))
                targetStep = new Point(Position.X - 1, Position.Y);
            else if (PreviousKs.IsKeyUp(Keys.Right) && CurrentKs.IsKeyDown(Keys.Right))
                targetStep = new Point(Position.X + 1, Position.Y);
            else if (PreviousKs.IsKeyUp(Keys.Up) && CurrentKs.IsKeyDown(Keys.Up))
                targetStep = new Point(Position.X, Position.Y - 1);
            else if (PreviousKs.IsKeyUp(Keys.Down) && CurrentKs.IsKeyDown(Keys.Down))
                targetStep = new Point(Position.X, Position.Y + 1);

            if (targetStep != Position)
                RaiseWalkEvent(new StepEventArgs(targetStep));
        }

        protected internal void RaiseStepEvent(StepEventArgs stepEventArgs)
        {
            if (Step != null)
                Step(this, stepEventArgs);
        }
        protected internal void RaiseWalkEvent(StepEventArgs stepEventArgs)
        {
            if (Walk != null)
                Walk(this, stepEventArgs);
        }


        public Texture2D GetXpBar(GraphicsDevice device)
        {
            if (redrawXpBar)
            {
                float percentage = (float)Xp / XpToNextLevel;
                const int barWidth = 192,
                          barHeight = 20;

                Texture2D texture2D = new Texture2D(device, barWidth, 20);

                Color[] pixels = new Color[barWidth * barHeight];
                for (int i = 0, total = barWidth * barHeight; i < total; i++)
                {
                    int y = i / barWidth,
                        x = i % barWidth;
                    float p = (float)x / barWidth;

                    // is pixel at border
                    if (y <= 1 || x <= 1 || x >= barWidth - 2 || y >= barHeight - 2)
                        pixels[i] = Color.DarkGreen;
                    else
                        pixels[i] = p > percentage ? Color.Transparent : Color.Green;

                }
                texture2D.SetData(pixels);
                XpBar = texture2D;
                redrawXpBar = false;
            }
            return XpBar;
        }
        public int CalculateXp(Creep.Creep kill)
        {
            int xp;
            if(Level < kill.Level)
            {
                xp = kill.Level*2;
            }else
            {
                xp = kill.Level;
            }
            return xp;
        }
    }
}
